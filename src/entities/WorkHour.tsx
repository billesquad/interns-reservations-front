export default interface WorkHour {
    workHourId: number,
    providerId: number,
    dayOfWeek: string,
    startTime: Date,
    endTime: Date,
}