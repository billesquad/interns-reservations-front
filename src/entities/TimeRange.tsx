import dayjs from "dayjs";

export default interface TimeRange {
    startTime: dayjs.Dayjs;
    endTime: dayjs.Dayjs;
}