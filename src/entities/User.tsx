import Address from "@/entities/Address.tsx";
export default interface User {
    providerId: number;
    addressId: number;
    status: string;
    firstName: string;
    lastName: string;
    birthDate: string;
    email: string;
    phoneNumber: string;
    specialization?: string;
    address?: Address | null;
}