export default interface Reservation {
    reservationId: number;
    customerId: number;
    offeringId: number;
    providerId: number;
    offeringName: string;
    price: number;
    currency: string;
    description: string;
    providerFullName: string;
    providerSpecialization: string;
    customerFullName: string;
    reservationDate: string;
    reservationDateEnd: string;
    status: string;
  }