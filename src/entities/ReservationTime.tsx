export default interface ReservationTime {
    reservationStartTime: Date,
    reservationEndTime: Date,
}