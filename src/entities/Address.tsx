export default interface Provider {
    addressId: number;
    country: string;
    city: string;
    street: string;
    zipCode: string;
    buildingNumber: string;
    apartmentNumber: string;
    isActive: string;
}