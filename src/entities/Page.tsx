export default interface Page<T> {
    content: T[],
    totalElements: number
}