export default interface OfferingSearchParams {
    page: number;
    size: number;
    sortParam: string;
    direction: string;
    nameSearch: string | null;
    priceFrom: number | null;
    priceTo: number | null;
    provider: string | null;
}