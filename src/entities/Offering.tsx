export default interface Offering {
    offeringId: number;
    providerId: number;
    providerFullName: string;
    providerSpecialization: string;
    offeringName: string;
    duration: number;
    price: number;
    currency: string;
    description: string;
  }