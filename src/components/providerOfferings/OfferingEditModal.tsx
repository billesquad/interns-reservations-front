import {Dispatch, SetStateAction} from "react";
import {
    Button,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    TextField,
    Select,
    MenuItem,
    InputLabel, FormControl, Divider
} from "@mui/material";
import ObjectData from "@/universalComponents/ObjectDataType";
import {Errors} from "@/components/providerOfferings/providerOfferingsHelpers";

interface OfferingModalProps {
    open: boolean;
    onClose: () => void;
    onSubmit: (data: ObjectData) => void;
    formValues: ObjectData;
    setFormValues: Dispatch<SetStateAction<ObjectData>>;
    errors: Errors;
    setErrors: (value: Errors) => void;
}

const OfferingEditModal = ({
                               open,
                               onClose,
                               onSubmit,
                               formValues,
                               setFormValues,
                               errors,
                               setErrors
                           }: OfferingModalProps) => {

    const validate = (): boolean => {
        const tempErrors: Errors = {};
        tempErrors.offeringName = formValues.offeringName ? "" : "Offering name is required.";
        tempErrors.description = formValues.description ? "" : "Description is required.";
        tempErrors.duration = parseInt(formValues.duration) > 0 ? "" : "Duration should be a positive number.";
        tempErrors.price = parseInt(formValues.price) > 0 ? "" : "Price should be a positive number.";
        tempErrors.currency = formValues.currency && formValues.currency.length >= 3 ? "" : "Currency should be at least 3 characters.";
        setErrors(tempErrors);

        return Object.values(tempErrors).every(err => err === "");
    }
    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement> | React.ChangeEvent<{ name?: string, value: unknown }>) => {
        const name = (event.target as HTMLInputElement).name;
        const value = (event.target as HTMLInputElement).value;
        setFormValues(prevState => ({...prevState as object, [name]: value}));
    };

    const handleSubmit = () => {
        if (validate()) {
            onSubmit(formValues);
        }
    };

    return (
        <Dialog open={open} onClose={onClose}>
            <DialogTitle>Edit Offering</DialogTitle>
            {formValues && <DialogContent>
                <Divider textAlign="left" sx={{p:"0.5rem"}}>Main Info</Divider>
                <TextField
                    margin="dense"
                    name="offeringName"
                    label="Offering Name"
                    fullWidth
                    variant="outlined"
                    InputLabelProps={{
                        shrink: true,
                    }}
                    value={formValues.offeringName}
                    onChange={handleInputChange}
                    required
                    error={!!errors.offeringName}
                    helperText={errors.offeringName}
                />
                <TextField
                    margin="dense"
                    name="description"
                    label="Description"
                    fullWidth
                    multiline
                    rows={4}
                    variant="outlined"
                    InputLabelProps={{
                        shrink: true,
                    }}
                    value={formValues.description}
                    onChange={handleInputChange}
                    required
                    error={!!errors.description}
                    helperText={errors.description}
                />
                <Divider textAlign="left" sx={{p:"0.5rem"}}>Detailed Info</Divider>
                <TextField
                    margin="dense"
                    name="duration"
                    label="Duration"
                    fullWidth
                    variant="outlined"
                    type="number"
                    InputLabelProps={{
                        shrink: true,
                    }}
                    value={formValues.duration}
                    onChange={handleInputChange}
                    required
                    error={!!errors.duration}
                    helperText={errors.duration}
                />
                <TextField
                    margin="dense"
                    name="price"
                    label="Price"
                    fullWidth
                    variant="outlined"
                    type="number"
                    InputLabelProps={{
                        shrink: true,
                    }}
                    value={formValues.price}
                    onChange={handleInputChange}
                    required
                    error={!!errors.price}
                    helperText={errors.price}
                />
                <FormControl fullWidth margin="dense">
                    <InputLabel id="currency-select-label">Currency</InputLabel>
                    <Select
                        labelId="currency-select-label"
                        name="currency"
                        label="Currency"
                        fullWidth
                        value={formValues.currency}
                        onChange={(event) => handleInputChange(event as React.ChangeEvent<HTMLInputElement>)}
                        required>
                        <MenuItem value={"PLN"}>PLN</MenuItem>
                        <MenuItem value={"EUR"}>EUR</MenuItem>
                        <MenuItem value={"USD"}>USD</MenuItem>
                    </Select>
                </FormControl>
            </DialogContent>}
            <DialogActions>
                <Button onClick={onClose} color="primary">
                    Cancel
                </Button>
                <Button onClick={handleSubmit} color="primary">
                    Save
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default OfferingEditModal;
