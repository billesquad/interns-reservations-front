import ObjectData from "@/universalComponents/ObjectDataType";
import getOfferingBody, {Errors} from "@/components/providerOfferings/providerOfferingsHelpers";
import {OFFERING_EMPTY_FORM} from "@/components/providerOfferings/providerOfferingsValues";
import SendApiRequest from "@/fetching/SendApiRequest";
import {CREATE_OFFERING} from "@/fetching/urls/external/providerApiUrls";

export const closeNewOfferingModal = (setOpenNewModal: (value: (((prevState: boolean) => boolean) | boolean)) => void, setNewFormData: (value: (((prevState: ObjectData) => ObjectData) | ObjectData)) => void, setNewErrors: (value: (((prevState: Errors) => Errors) | Errors)) => void) => {
    return () => {
        setOpenNewModal(false);
        setNewFormData(OFFERING_EMPTY_FORM)
        setNewErrors({})
    };
}
export const newOffering = (openSnackbar: (message: string, severity: ("success" | "error" | "info" | "warning")) => void, setForceUpdate: (value: (((prevState: boolean) => boolean) | boolean)) => void, forceUpdate: boolean) => {
    return (data: ObjectData) => {
        SendApiRequest({
            partOfUrl: CREATE_OFFERING,
            typeOfRequest: "POST",
            body: getOfferingBody(data)
        }, openSnackbar).then(() => setForceUpdate(!forceUpdate));
    };
}