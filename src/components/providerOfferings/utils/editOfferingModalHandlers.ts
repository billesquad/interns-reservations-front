import ObjectData from "@/universalComponents/ObjectDataType";
import SendApiRequest from "@/fetching/SendApiRequest";
import {UPDATE_OFFERING_BY_ID} from "@/fetching/urls/external/providerApiUrls";
import getOfferingBody, {Errors} from "@/components/providerOfferings/providerOfferingsHelpers";

export const openEditOfferingModal = (setOfferingToEdit: (value: (((prevState: ObjectData) => ObjectData) | ObjectData)) => void, setOpenEditModal: (value: (((prevState: boolean) => boolean) | boolean)) => void) => {
    return (data: ObjectData) => {
        setOfferingToEdit(data);
        setOpenEditModal(true);
    };
}
export const updateOffering = (openSnackbar: (message: string, severity: ("success" | "error" | "info" | "warning")) => void, setOpenEditModal: (value: (((prevState: boolean) => boolean) | boolean)) => void, setForceUpdate: (value: (((prevState: boolean) => boolean) | boolean)) => void, forceUpdate: boolean) => {
    return (data: ObjectData) => {
        SendApiRequest({
            partOfUrl: UPDATE_OFFERING_BY_ID(data.offeringId),
            typeOfRequest: "PUT",
            body: getOfferingBody(data)
        }, openSnackbar);
        setOpenEditModal(false);
        setForceUpdate(!forceUpdate);
    };
}
export const closeEditModal = (setOpenEditModal: (value: (((prevState: boolean) => boolean) | boolean)) => void, setEditErrors: (value: (((prevState: Errors) => Errors) | Errors)) => void) => {
    return () => {
        setOpenEditModal(false)
        setEditErrors({})
    };
}