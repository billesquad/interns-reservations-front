import SendApiRequest from "@/fetching/SendApiRequest";
import ObjectData from "@/universalComponents/ObjectDataType";
import {DELETE_OFFERING_BY_ID} from "@/fetching/urls/external/providerApiUrls";

export const removeOffering = (offeringToRemove: string | undefined, openSnackbar: (message: string, severity: ("success" | "error" | "info" | "warning")) => void, setForceUpdate: (value: (((prevState: boolean) => boolean) | boolean)) => void, forceUpdate: boolean, setOpenRemoveModal: (value: (((prevState: boolean) => boolean) | boolean)) => void) => {
    return () => {
        if (offeringToRemove) {
            SendApiRequest({
                partOfUrl: DELETE_OFFERING_BY_ID(offeringToRemove), typeOfRequest: "PATCH"
            }, openSnackbar).then(() => setForceUpdate(!forceUpdate));
        }
        setOpenRemoveModal(false);
    };
}

export const removeModalOpen = (setOfferingToRemove: (value: (((prevState: (string | undefined)) => (string | undefined)) | string | undefined)) => void, setOpenRemoveModal: (value: (((prevState: boolean) => boolean) | boolean)) => void) => {
    return (data: ObjectData) => {
        setOfferingToRemove(data.offeringId);
        setOpenRemoveModal(true);
    };
}