import {useContext} from 'react';
import {
    Dialog, DialogTitle, DialogContent, DialogActions, Button,
    TextField, InputAdornment, InputLabel, Select, MenuItem, FormControl, FormHelperText, Divider
} from '@mui/material';
import ObjectData from "@/universalComponents/ObjectDataType";
import useUserContext from "@/components/auth/useUserContext";
import {OFFERING_EMPTY_FORM} from "@/components/providerOfferings/providerOfferingsValues";
import {Errors} from "@/components/providerOfferings/providerOfferingsHelpers";


interface OfferingCreationModalProps {
    isOpen: boolean;
    onClose: () => void;
    onSubmit: (data: ObjectData) => void;
    newFormData: ObjectData;
    setNewFormData: (data: ObjectData) => void;
    errors: Errors;
    setErrors: (value: Errors) => void;

}

const OfferingCreationModal = ({isOpen, onClose, onSubmit, newFormData, setNewFormData, errors, setErrors}: OfferingCreationModalProps) => {
    const {userId} = useContext(useUserContext)

    const validate = (): boolean => {
        const tempErrors: Errors = {};
        tempErrors.offeringName = newFormData.offeringName ? "" : "Offering name is required.";
        tempErrors.description = newFormData.description ? "" : "Description is required.";
        tempErrors.duration = parseInt(newFormData.duration) > 0 ? "" : "Duration should be a positive number.";
        tempErrors.price = parseInt(newFormData.price) > 0 ? "" : "Price should be a positive number.";
        tempErrors.currency = newFormData.currency && newFormData.currency.length >= 3 ? "" : "Currency should be at specified.";
        setNewFormData({...newFormData, providerId: userId as unknown as string})
        setErrors(tempErrors);

        return Object.values(tempErrors).every(err => err === "");
    }

    const handleSubmit = () => {
        if (validate()) {
            onSubmit({...newFormData, providerId: userId as unknown as string});
            onClose();
            setNewFormData(OFFERING_EMPTY_FORM)
        }
    };

    return (
        <Dialog open={isOpen} onClose={onClose}>
            <DialogTitle>Create Offering</DialogTitle>
            <DialogContent>
                <Divider textAlign="left" sx={{p:"0.5rem"}}>Main Info</Divider>
                <TextField
                    fullWidth
                    margin="dense"
                    label="Offering Name"
                    variant="outlined"
                    value={newFormData.offeringName}
                    onChange={(e) => setNewFormData({...newFormData, offeringName: e.target.value})}
                    error={!!errors.offeringName}
                    helperText={errors.offeringName}/>
                <TextField
                    fullWidth
                    multiline
                    maxRows={4}
                    rows={4}
                    margin="dense"
                    label="Description"
                    variant="outlined"
                    value={newFormData.description}
                    onChange={(e) => setNewFormData({...newFormData, description: e.target.value})}
                    error={!!errors.description}
                    helperText={errors.description}
                />
                <Divider textAlign="left" sx={{p:"0.5rem"}}>Detailed Info</Divider>
                <TextField
                    fullWidth
                    margin="dense"
                    label="Duration"
                    variant="outlined"
                    type="number"
                    value={newFormData.duration}
                    onChange={(e) => setNewFormData({...newFormData, duration: e.target.value})}
                    error={!!errors.duration}
                    helperText={errors.duration}
                    InputProps={{
                        endAdornment: <InputAdornment position="end">minutes</InputAdornment>,
                    }}
                />
                <TextField
                    fullWidth
                    margin="dense"
                    label="Price"
                    variant="outlined"
                    type="number"
                    value={newFormData.price}
                    onChange={(e) => setNewFormData({...newFormData, price: e.target.value})}
                    error={!!errors.price}
                    helperText={errors.price}
                    InputProps={{
                        startAdornment: <InputAdornment position="start">$</InputAdornment>,
                    }}
                />
                <FormControl fullWidth margin="dense" error={!!errors.currency}>
                    <InputLabel id="currency-select-label">Currency</InputLabel>
                    <Select
                        labelId="currency-select-label"
                        name="currency"
                        label="Currency"
                        fullWidth
                        value={newFormData.currency}
                        onChange={(e) => setNewFormData({...newFormData, currency: e.target.value})}
                        required
                        error={!!errors.currency}
                    >
                        <MenuItem value={"PLN"}>PLN</MenuItem>
                        <MenuItem value={"EUR"}>EUR</MenuItem>
                        <MenuItem value={"USD"}>USD</MenuItem>
                    </Select>
                    <FormHelperText>{errors.currency}</FormHelperText>
                </FormControl>
            </DialogContent>
            <DialogActions>
                <Button onClick={onClose} color="primary">Cancel</Button>
                <Button onClick={handleSubmit} color="primary">Submit</Button>
            </DialogActions>
        </Dialog>
    );
};

export default OfferingCreationModal;
