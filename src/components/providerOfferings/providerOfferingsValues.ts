import {UrlParams} from "@/fetching/SendApiRequest";
import ObjectData from "@/universalComponents/ObjectDataType";

export const PROVIDER_OFFERING_SORT_PARAMS = {"duration": "Duration", "price": "Price", "currency": "Currency"}

export const OFFERINGS_NAMES = {
    "offeringId": "Offering ID",
    "providerId": "Provider ID",
    "providerFullName": "Provider Name",
    "providerSpecialization": "Provider Specialization",
    "offeringName": "Offering Name",
    "duration": "Offering Duration",
    "price": "Offering Price",
    "currency": "Offering Currency",
    "description": "Offering Description",
}
export const INITIAL_URL_PARAMETERS_OFFERINGS_PROVIDER: UrlParams = {
    page: "0",
    size: "3",
    sortParam: "price",
    direction: "ASC",
}
export const OFFERING_EMPTY_FORM: ObjectData = {
    providerId: "",
    offeringName: "",
    description: "",
    duration: "",
    price: "",
    currency: ""
}
