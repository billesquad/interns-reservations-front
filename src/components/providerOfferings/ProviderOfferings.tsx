import useFetchDataFromUrlParams from "@/universalComponents/universalSideMenu/useFetchByUrlParams";
import {useContext, useState} from "react";
import useUserContext from "@/components/auth/useUserContext";
import {useSnackbar} from "@/informationSnackbar/SnackbarProvider";
import ObjectData from "@/universalComponents/ObjectDataType";
import {UrlParams} from "@/fetching/SendApiRequest";
import {Box, Button, Divider, Grid} from "@mui/material";
import BrowseSideMenu from "@/universalComponents/universalSideMenu/BrowseSideMenu";
import ManyItemDisplay from "@/universalComponents/ManyItemDisplay";
import {
    INITIAL_URL_PARAMETERS_OFFERINGS_PROVIDER,
    OFFERING_EMPTY_FORM,
    OFFERINGS_NAMES,
    PROVIDER_OFFERING_SORT_PARAMS
} from "@/components/providerOfferings/providerOfferingsValues";
import ConfirmationModal from "@/universalComponents/ConfirmationModal";
import OfferingEditModal from "@/components/providerOfferings/OfferingEditModal";
import OfferingNewModal from "@/components/providerOfferings/OfferingNewModal";
import {Errors} from "@/components/providerOfferings/providerOfferingsHelpers";
import {removeModalOpen, removeOffering} from "@/components/providerOfferings/utils/removeOfferingModalHandlers";
import {
    closeEditModal,
    openEditOfferingModal,
    updateOffering
} from "@/components/providerOfferings/utils/editOfferingModalHandlers";
import {closeNewOfferingModal, newOffering} from "@/components/providerOfferings/utils/newOfferingModalHandlers";
import {GET_OFFERINGS_BY_PROVIDER_ID} from "@/fetching/urls/external/providerApiUrls";




const CustomerReservations = () => {
    const {userId} = useContext(useUserContext);
    const {openSnackbar} = useSnackbar();

    const [openRemoveModal, setOpenRemoveModal] = useState(false);
    const [openEditModal, setOpenEditModal] = useState(false);
    const [totalElements, setTotalElements] = useState<number>(10);
    const [openNewModal, setOpenNewModal] = useState(false);
    const [forceUpdate, setForceUpdate] = useState(true);
    const [newFormData, setNewFormData] = useState<ObjectData>(OFFERING_EMPTY_FORM);
    const [fetchedData, setFetchedData] = useState<ObjectData[]>();
    const [offeringToRemove, setOfferingToRemove] = useState<string>();
    const [offeringToEdit, setOfferingToEdit] = useState<ObjectData>({});
    const [componentUrlParameters, setComponentUrlParameters] = useState<UrlParams>(INITIAL_URL_PARAMETERS_OFFERINGS_PROVIDER);
    const [editErrors, setEditErrors] = useState<Errors>({});
    const [newErrors, setNewErrors] = useState<Errors>({});

    const handleRemoveClose = () => setOpenRemoveModal(false);
    const handleEditClose = closeEditModal(setOpenEditModal, setEditErrors);
    const handleNewClose = closeNewOfferingModal(setOpenNewModal, setNewFormData, setNewErrors);

    const handleRemoveConfirm = removeOffering(offeringToRemove, openSnackbar, setForceUpdate, forceUpdate, setOpenRemoveModal);
    const handleEditConfirm = updateOffering(openSnackbar, setOpenEditModal, setForceUpdate, forceUpdate);
    const handleNewConfirm = newOffering(openSnackbar, setForceUpdate, forceUpdate);
    const handleRemoveModalOpen = removeModalOpen(setOfferingToRemove, setOpenRemoveModal);
    const handleEditModalOpen = openEditOfferingModal(setOfferingToEdit, setOpenEditModal);

    useFetchDataFromUrlParams(componentUrlParameters, GET_OFFERINGS_BY_PROVIDER_ID((userId as number).toString()), setFetchedData, forceUpdate, setTotalElements);

    const manyDisplayMetadata = {
        attributeNames: OFFERINGS_NAMES,
        excludeKeys: ["offeringId", "providerId", "offeringName"],
        valueFormatters: {duration: (value: string) => value + " min",},
        menuItems: {"Remove offering": handleRemoveModalOpen, "Edit Offering": handleEditModalOpen,},
        onClickParameter: "offeringId",
        itemName: "offeringName",
        descriptionKey: "description",
    };

    return (
        <Box sx={{
            display: 'flex',
            flexDirection: 'column',
            gap: 5,
            flexGrow: 0,
            alignItems: 'center',
            justifyContent: 'space-between'
        }}>
            {userId && (
                <Box sx={{width: "100%"}}>
                    <Grid
                        container
                        direction="row"
                        justifyContent="space-between"
                        alignItems="center"
                        spacing={3}
                        sx={{mb: "2rem"}}
                    >
                        <Grid item xs={2}>
                            <Button sx={{mb: "0.5rem"}} variant="contained" size="large"
                                    onClick={() => setOpenNewModal(true)}>Create New Offering</Button>
                        </Grid>
                        <BrowseSideMenu componentUrlParameters={componentUrlParameters}
                                        setComponentUrlParameters={setComponentUrlParameters}
                                        sortParameters={PROVIDER_OFFERING_SORT_PARAMS}
                                        totalElements={totalElements}/>
                    </Grid>
                    <Divider/>
                </Box>
            )}
            {fetchedData && <ManyItemDisplay data={fetchedData} metadata={manyDisplayMetadata}/>}
            <ConfirmationModal open={openRemoveModal} onClose={handleRemoveClose} message="Remove offering"
                               onConfirm={handleRemoveConfirm}/>
            <OfferingEditModal open={openEditModal} onClose={handleEditClose} onSubmit={handleEditConfirm}
                               formValues={offeringToEdit} setFormValues={setOfferingToEdit}
                                errors={editErrors} setErrors={setEditErrors}/>
            <OfferingNewModal isOpen={openNewModal} onClose={handleNewClose} onSubmit={handleNewConfirm} newFormData={newFormData} setNewFormData={setNewFormData}
            errors={newErrors} setErrors={setNewErrors}/>
        </Box>
    )
}

export default CustomerReservations;