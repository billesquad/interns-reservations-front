import ObjectData from "@/universalComponents/ObjectDataType";

const getOfferingBody = (data: ObjectData) => ({
    providerId: data.providerId,
    offeringName: data.offeringName,
    description: data.description,
    duration: data.duration,
    price: data.price,
    currency: data.currency,
});

export interface Errors {
    [key: string]: string;
}

export default getOfferingBody;