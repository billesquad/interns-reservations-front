import User from "@/entities/User.tsx";
import {useEffect} from "react";
import SendApiRequest, {RequestConfig} from "@/fetching/SendApiRequest.ts";
import Address from "@/entities/Address.tsx";
import UserRoles from "@/enums/UserRoles.ts";

function FetchFormData(userId: string | number | undefined, userRole: UserRoles | undefined, urlRole: string, setUser: (value: (((prevState: (User | null)) => (User | null)) | User | null)) => void, setFormData: (value: (((prevState: (User | null)) => (User | null)) | User | null)) => void, setError: (value: (((prevState: (string | null)) => (string | null)) | string | null)) => void, user: User | null, setAddress: (value: (((prevState: (Address | null)) => (Address | null)) | Address | null)) => void, address: Address | null) {
    useEffect(() => {
        if (!userId || !userRole) return;

        const fetchUserData = async () => {
            const requestConfig: RequestConfig = {
                partOfUrl: `/common/${urlRole}/${userId}`,
            };

            try {
                const response = await SendApiRequest(requestConfig) as unknown as User;
                console.log("Received user data:", response); // Logging
                setUser(response);
                setFormData(response);
            } catch (error) {
                console.error('Error fetching user data:', error);
                setError('Error fetching user data');
            }
        };

        fetchUserData();
    }, [userId, userRole, urlRole]);

    //address
    useEffect(() => {
        if (!user || !user.addressId) return;

        const addressId = user.addressId;

        const fetchAddressData = async () => {
            const requestConfig: RequestConfig = {
                partOfUrl: `/common/address/${addressId}`,
            };

            try {
                const addressResponse = await SendApiRequest(requestConfig) as unknown as Address;
                console.log("Fetched address data:", addressResponse); // Logging
                setAddress(addressResponse);
            } catch (error) {
                console.error('Error fetching address data:', error);
                setError('Error fetching address data');
            }
        };

        fetchAddressData();
    }, [user]);

    console.log("Current user state:", user); // Logging
    console.log("Current address state:", address); // Logging

    useEffect(() => {
        if (user && address) {
            setFormData({
                ...user,
                address: address
            });
        }
    }, [user, address]);
}

export default FetchFormData;