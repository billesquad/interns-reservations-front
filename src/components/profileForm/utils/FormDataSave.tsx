import User from "@/entities/User.tsx";
import SendApiRequest from "@/fetching/SendApiRequest.ts";
import Address from "@/entities/Address.tsx";

interface RequestConfig {
    partOfUrl: string;
    typeOfRequest?: string;
    body?: Record<string, unknown>;
}


function GetDataSave(validationErrors: { [p: string]: string }, formData: User | {
    providerId: number | undefined;
    addressId: number | undefined;
    status: string | undefined;
    firstName: string | undefined;
    lastName: string | undefined;
    birthDate: string | undefined;
    email: string | undefined;
    phoneNumber: string | undefined;
    specialization?: string | undefined;
    address?: Address | null | undefined
} | null, isEditing: boolean, putProfileUserUrl: string, setUser: (value: (((prevState: (User | null)) => (User | null)) | User | null)) => void, setIsEditing: (value: (((prevState: boolean) => boolean) | boolean)) => void, setError: (value: (((prevState: (string | null)) => (string | null)) | string | null)) => void, setSuccessMessage: (value: (((prevState: (string | null)) => (string | null)) | string | null)) => void, setErrorMessage: (value: (((prevState: (string | null)) => (string | null)) | string | null)) => void, isAddressEditing: boolean, user: User | null, setAddress: (value: (((prevState: (Address | null)) => (Address | null)) | Address | null)) => void, setIsAddressEditing: (value: (((prevState: boolean) => boolean) | boolean)) => void) {
    const handleSave = async () => {
        if (Object.keys(validationErrors).length > 0) {
            // Jeśli są błędy walidacji, nie kontynuuj zapisywania
            return;
        }
        if (!formData) return;

        // Aktualizacja danych użytkownika
        if (isEditing) {
            const providerData = {
                firstName: formData.firstName,
                lastName: formData.lastName,
                birthDate: formData.birthDate,
                phoneNumber: formData.phoneNumber,
                specialization: formData.specialization,
            };

            const requestConfig: RequestConfig = {
                partOfUrl: putProfileUserUrl,
                typeOfRequest: 'PUT',
                body: providerData as Record<string, unknown>,
            };

            try {
                const response = await SendApiRequest(requestConfig) as unknown as User;
                setUser(response);
                setIsEditing(false);
                setError(null);
                setSuccessMessage("User data updated successfully.");

            } catch (error) {
                console.error('Error updating user data:', error);
                setError('Error updating user data');
                setErrorMessage("An error occurred while updating user's data.");
            }
        }

        // Aktualizacja adresu
        if (isAddressEditing && formData?.address) {
            const addressData = {
                street: formData.address.street,
                city: formData.address.city,
                buildingNumber: formData.address.buildingNumber,
                apartmentNumber: formData.address.apartmentNumber,
                zipCode: formData.address.zipCode,
                country: formData.address.country,
            };

            const addressRequestConfig: RequestConfig = {
                partOfUrl: `/common/address/update/${user?.addressId}`,
                typeOfRequest: 'PUT',
                body: addressData,
            };

            try {
                const addressResponse = await SendApiRequest(addressRequestConfig) as unknown as Address;
                setAddress(addressResponse);
                setIsAddressEditing(false);
                setError(null);
            } catch (error) {
                console.error('Error updating address data:', error);
                setError('Error updating address data');
            }
        }
    }
    const handleAddressSave = async () => {
        if (Object.keys(validationErrors).length > 0) {
            return;
        }
        console.log("handleAddressSave is called")
        console.log("formData?.address:", formData?.address);
        console.log("user?.addressId:", user?.addressId);
        if (!formData?.address) return;

        const addressData = {
            street: formData.address.street,
            city: formData.address.city,
            buildingNumber: formData.address.buildingNumber,
            apartmentNumber: formData.address.apartmentNumber,
            zipCode: formData.address.zipCode,
            country: formData.address.country,
        };

        const addressRequestConfig: RequestConfig = {
            partOfUrl: `/common/address/update/${user?.addressId}`,
            typeOfRequest: 'PUT',
            body: addressData,
        };

        try {
            const addressResponse = await SendApiRequest(addressRequestConfig) as unknown as Address;
            setAddress(addressResponse); // Aktualizacja stanu address po udanej aktualizacji
            setIsAddressEditing(false);
            setError(null);
            setSuccessMessage("The address has been successfully updated.");

        } catch (error) {
            console.error('Error updating address data:', error);
            setError('Error updating address data');
            setErrorMessage("An error occurred while updating the address.");
        }

    };
    return {handleSave, handleAddressSave};
}
export default GetDataSave;