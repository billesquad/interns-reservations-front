import User from "@/entities/User.tsx";

function GetInputChangeHandle(validatePhoneNumber: (phoneNumber: string) => boolean, setValidationErrors: (value: (((prevState: {
    [p: string]: string
}) => { [p: string]: string }) | {
    [p: string]: string
})) => void, validateBirthDate: (birthDate: string) => boolean, validateFirstName: (firstName: string) => boolean, setFormData: (value: (((prevState: (User | null)) => (User | null)) | User | null)) => void) {
    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, isAddressField?: boolean) => {
        const {name, value} = event.target;

        if (name === "phoneNumber" && !validatePhoneNumber(value)) {
            setValidationErrors(prevErrors => ({
                ...prevErrors,
                phoneNumber: "Phone number must have at least 7 digits."
            }));
        } else {
            setValidationErrors(prevErrors => {
                const newErrors = {...prevErrors};
                delete newErrors.phoneNumber;
                return newErrors;
            });
        }

        if (name === "birthDate" && !validateBirthDate(value)) {
            setValidationErrors(prevErrors => ({
                ...prevErrors,
                birthDate: "You must be at least 18 years old."
            }));
        } else {
            setValidationErrors(prevErrors => {
                const newErrors = {...prevErrors};
                delete newErrors.birthDate;
                return newErrors;
            });
        }

        if (name === "firstName" && !validateFirstName(value)) {
            setValidationErrors(prevErrors => ({
                ...prevErrors,
                firstName: "First name must contain only letters and be between 2 and 255 characters."
            }));
        } else {
            setValidationErrors(prevErrors => {
                const newErrors = {...prevErrors};
                delete newErrors.firstName;
                return newErrors;
            });
        }

        if (name === "lastName") {
            if (value.trim() === "") {
                setValidationErrors(prevErrors => ({
                    ...prevErrors,
                    lastName: "Last name field must be filled in."
                }));
            } else if (value.length < 2 || value.length > 255) {
                setValidationErrors(prevErrors => ({
                    ...prevErrors,
                    lastName: "Last name must be between 2 and 255 characters."
                }));
            } else if (!/^[a-zA-Z]+$/.test(value)) {
                setValidationErrors(prevErrors => ({
                    ...prevErrors,
                    lastName: "Last name must contain only letters."
                }));
            } else {
                setValidationErrors(prevErrors => {
                    const newErrors = {...prevErrors};
                    delete newErrors.lastName;
                    return newErrors;
                });
            }
        }

        if (isAddressField) {
            setFormData(prevState => {
                if (!prevState || !prevState.address) return null;
                return {
                    ...prevState,
                    address: {
                        ...prevState.address,
                        [name]: value || ''
                    }
                };
            });

        } else {
            setFormData(prevState => {
                if (!prevState) return null;
                return {
                    ...prevState,
                    [name]: value || ''
                };
            });
        }
    };
    return handleInputChange;
}
export default GetInputChangeHandle;
