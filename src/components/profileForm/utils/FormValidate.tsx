function Validation() {
    const validatePhoneNumber = (phoneNumber: string) => {
        const regex = /^\d{7,}$/;
        return regex.test(phoneNumber);
    };
    const validateFirstName = (firstName: string) => {
        const regex = /^[a-zA-Z]{2,255}$/;
        return regex.test(firstName);
    };

    const validateBirthDate = (birthDate: string) => {
        const today = new Date();
        const birthDateObj = new Date(birthDate);
        let age = today.getFullYear() - birthDateObj.getFullYear();
        const monthDifference = today.getMonth() - birthDateObj.getMonth();

        if (monthDifference < 0 || (monthDifference === 0 && today.getDate() < birthDateObj.getDate())) {
            age--;
        }

        return age >= 18;
    };
    return {validatePhoneNumber, validateFirstName, validateBirthDate};
}

export default Validation;