import {Snackbar} from "@mui/material";

function GetUserFormSnack(successMessage: string | null, setSuccessMessage: (value: (((prevState: (string | null)) => (string | null)) | string | null)) => void, errorMessage: string | null, setErrorMessage: (value: (((prevState: (string | null)) => (string | null)) | string | null)) => void, cancelMessage: string | null, setCancelMessage: (value: (((prevState: (string | null)) => (string | null)) | string | null)) => void) {
    return <>
        <Snackbar
            open={!!successMessage}
            autoHideDuration={6000}
            onClose={() => setSuccessMessage(null)}
            message={successMessage}
            anchorOrigin={{vertical: 'top', horizontal: 'center'}}
            ContentProps={{style: {backgroundColor: 'green'}}}
        />

        <Snackbar
            open={!!errorMessage}
            autoHideDuration={6000}
            onClose={() => setErrorMessage(null)}
            message={errorMessage}
            anchorOrigin={{vertical: 'top', horizontal: 'center'}}
            ContentProps={{style: {backgroundColor: 'red'}}}
        />

        <Snackbar
            open={!!cancelMessage}
            autoHideDuration={6000}
            onClose={() => setCancelMessage(null)}
            message={cancelMessage}
            anchorOrigin={{vertical: 'top', horizontal: 'center'}}
            ContentProps={{style: {backgroundColor: 'orange'}}}
        />
    </>;
}

export default GetUserFormSnack;