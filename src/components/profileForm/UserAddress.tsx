import React from "react";
import {Autocomplete, Box, Button, CircularProgress, TextField, Typography} from "@mui/material";
import countries from "i18n-iso-countries";
import Address from "@/entities/Address.tsx";
import User from "@/entities/User.tsx";

function GetUserAddress(address: Address | null, isAddressEditing: boolean, countryList: {
    code: string;
    name: string
}[], formData: User | null, handleInputChange: (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, isAddressField?: boolean) => void, handleAddressSave: () => Promise<void>, handleCancel: () => void, handleAddressEdit: () => void) {
    return <Box flex={1} marginLeft={2}>
        <Typography variant="h5" gutterBottom align="center">
            Address
        </Typography>
        {!address && <CircularProgress style={{margin: '20px auto', display: 'block'}}/>}
        {address && (
            <Box marginBottom="20px">
                {isAddressEditing ? (
                    <Autocomplete
                        options={countryList}
                        getOptionLabel={(option) => option.name}
                        value={countryList.find(country => country.code === formData?.address?.country) || null}
                        onChange={(_, newValue) => {
                            const syntheticEvent = {
                                target: {name: 'country', value: newValue ? newValue.code : ''},
                            } as React.ChangeEvent<HTMLInputElement>;
                            handleInputChange(syntheticEvent, true);
                        }} isOptionEqualToValue={(option, value) => option.code === value.code}
                        renderInput={(params) => (
                            <TextField
                                {...params}
                                name="country"
                                label="Country"
                                fullWidth
                                margin="normal"
                            />
                        )}
                    />
                ) : (
                    <TextField
                        name="country"
                        label="Country"
                        value={countries.getName(formData?.address?.country || '', 'en') || ''}
                        fullWidth
                        margin="normal"
                        InputProps={{
                            readOnly: true,
                        }}
                    />
                )}
                <TextField
                    name="street"
                    label="Street"
                    value={formData?.address?.street || ''}
                    fullWidth
                    margin="normal"
                    InputProps={{
                        readOnly: !isAddressEditing,
                    }}
                    onChange={(e) => handleInputChange(e, true)}
                />
                <TextField
                    name="city"
                    label="City"
                    value={formData?.address?.city || ''}
                    fullWidth
                    margin="normal"
                    InputProps={{
                        readOnly: !isAddressEditing,
                    }}
                    onChange={(e) => handleInputChange(e, true)}
                />
                <TextField
                    name="buildingNumber"
                    label="Building Number"
                    value={formData?.address?.buildingNumber || ''}
                    fullWidth
                    margin="normal"
                    InputProps={{
                        readOnly: !isAddressEditing,
                    }}
                    onChange={(e) => handleInputChange(e, true)}
                />
                <TextField
                    name="apartmentNumber"
                    label="Apartment Number"
                    value={formData?.address?.apartmentNumber || ''}
                    fullWidth
                    margin="normal"
                    InputProps={{
                        readOnly: !isAddressEditing,
                    }}
                    onChange={(e) => handleInputChange(e, true)}
                />
                <TextField
                    name="zipCode"
                    label="Zip Code"
                    value={formData?.address?.zipCode || ''}
                    fullWidth
                    margin="normal"
                    InputProps={{
                        readOnly: !isAddressEditing,
                    }}
                    onChange={(e) => handleInputChange(e, true)}
                />
            </Box>
        )}
        <Box marginTop="20px" textAlign="center">
            {isAddressEditing ? (
                <>
                    <Button variant="contained" color="primary" onClick={handleAddressSave}
                            style={{marginRight: '10px'}}>
                        Save Address
                    </Button>
                    <Button variant="contained" onClick={handleCancel}>
                        Cancel
                    </Button>
                </>
            ) : (
                <Button variant="contained" color="primary" onClick={handleAddressEdit}>
                    Edit Address
                </Button>
            )}
        </Box>
    </Box>;
}

export default GetUserAddress;