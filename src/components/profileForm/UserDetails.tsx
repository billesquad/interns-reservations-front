import User from "@/entities/User.tsx";
import React from "react";
import {Box, Button, CircularProgress, TextField, Typography} from "@mui/material";
import UserRoles from "@/enums/UserRoles.ts";


function GetUserDetails(user: User | null, error: string | null, formData: User | null, isEditing: boolean, handleInputChange: (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, isAddressField?: boolean) => void, validationErrors: {
    [p: string]: string
}, userRole: UserRoles, handleSave: () => Promise<void>, handleCancel: () => void, handleEdit: () => void) {
    return <Box flex={1} marginRight={2}>
        <Typography variant="h5" gutterBottom align="center">
            User's details
        </Typography>
        {!user && !error && <CircularProgress style={{margin: '20px auto', display: 'block'}}/>}
        {user && (
            <Box marginBottom="20px">
                <TextField
                    name="firstName"
                    label="First Name"
                    value={formData?.firstName || ''}
                    fullWidth
                    margin="normal"
                    InputProps={{
                        readOnly: !isEditing,
                    }}
                    onChange={handleInputChange}
                    error={!!validationErrors.firstName}
                    helperText={validationErrors.firstName}
                />
                <TextField
                    name="lastName"
                    label="Last Name"
                    value={formData?.lastName || ''}
                    fullWidth
                    margin="normal"
                    InputProps={{
                        readOnly: !isEditing,
                    }}
                    onChange={handleInputChange}
                    error={!!validationErrors.lastName}
                    helperText={validationErrors.lastName}
                />
                <TextField
                    name="email"
                    label="Email"
                    value={formData?.email || ''}
                    fullWidth
                    margin="normal"
                    InputProps={{
                        readOnly: true,
                    }}

                />
                <TextField
                    name="phoneNumber"
                    label="Phone Number"
                    value={formData?.phoneNumber || ''}
                    fullWidth
                    margin="normal"
                    InputProps={{
                        readOnly: !isEditing,
                    }}
                    onChange={handleInputChange}
                    error={!!validationErrors.phoneNumber}
                    helperText={validationErrors.phoneNumber}
                />
                <TextField
                    name="birthDate"
                    label="Birth Date"
                    type="date"
                    value={formData?.birthDate || ''}
                    fullWidth
                    margin="normal"
                    InputProps={{
                        readOnly: !isEditing,
                    }}
                    onChange={handleInputChange}
                    error={!!validationErrors.birthDate}
                    helperText={validationErrors.birthDate}
                    InputLabelProps={{
                        shrink: true,
                    }}
                />

                {userRole === 'ROLE_PROVIDER' && (
                    <TextField
                        name="specialization"
                        label="Specialization"
                        value={formData?.specialization || ''}
                        fullWidth
                        margin="normal"
                        InputProps={{
                            readOnly: !isEditing,
                        }}
                        onChange={handleInputChange}
                    />
                )}
            </Box>
        )}
        <Box marginTop="20px" textAlign="center">
            {isEditing ? (
                <>
                    <Button variant="contained" color="primary" onClick={handleSave} style={{marginRight: '10px'}}>
                        Save
                    </Button>
                    <Button variant="contained" onClick={handleCancel}>
                        Cancel
                    </Button>
                </>
            ) : (
                <Button variant="contained" color="primary" onClick={handleEdit}>
                    Edit details
                </Button>
            )}
        </Box>
    </Box>;
}

export default GetUserDetails;



