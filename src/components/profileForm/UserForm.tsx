// @ts-ignore
import React, {useContext, useState} from 'react';
import {Box, Paper} from '@mui/material';
import useUserContext from '@/components/auth/useUserContext';
import {UPDATE_CUSTOMER_BY_ID} from "@/fetching/urls/external/customerApiUrls";
import {UPDATE_PROVIDER_BY_ID} from "@/fetching/urls/external/providerApiUrls";
import countries from 'i18n-iso-countries';
import en from 'i18n-iso-countries/langs/en.json';
import User from "@/entities/User.tsx";
import Address from "@/entities/Address.tsx";
import GetUserDetails from "@/components/profileForm/UserDetails.tsx";
import GetUserAddress from "@/components/profileForm/UserAddress.tsx";
import UserRoles from "@/enums/UserRoles.ts";
import GetUserFormSnack from "@/components/profileForm/utils/FormSnackbar.tsx";
import Validation from "@/components/profileForm/utils/FormValidate.tsx";
import FetchFormData from "@/components/profileForm/utils/FormFetchFun.tsx";
import GetDataSave from "@/components/profileForm/utils/FormDataSave.tsx";
import GetInputChangeHandle from "@/components/profileForm/utils/FormInputChange.tsx";

countries.registerLocale(en);



const UserForm = () => {
    const {userId, userRole} = useContext(useUserContext) || {};
    const [user, setUser] = useState<User | null>(null);
    const [address, setAddress] = useState<Address | null>(null);
    const [error, setError] = useState<string | null>(null);
    const [isEditing, setIsEditing] = useState(false);
    const [formData, setFormData] = useState<User | null>(null);
    const [isAddressEditing, setIsAddressEditing] = useState(false);
    const [successMessage, setSuccessMessage] = useState<string | null>(null);
    const [errorMessage, setErrorMessage] = useState<string | null>(null);
    const [cancelMessage, setCancelMessage] = useState<string | null>(null);
    const [validationErrors, setValidationErrors] = useState<{ [key: string]: string }>({});
    const countryList = Object.entries(countries.getNames('en')).map(([code, name]) => ({code, name,}));
    const urlRole = userRole === 'ROLE_CUSTOMER' ? 'customer' : 'provider';
    const putProfileUserUrl = userRole === 'ROLE_CUSTOMER' ? UPDATE_CUSTOMER_BY_ID(userId as unknown as string)
        : UPDATE_PROVIDER_BY_ID(userId as unknown as string);

    FetchFormData(userId, userRole as UserRoles, urlRole, setUser, setFormData, setError, user, setAddress, address);

    const handleEdit = () => {
        setIsEditing(true);
    };

    const {
        handleSave,
        handleAddressSave
    } = GetDataSave(validationErrors, formData, isEditing, putProfileUserUrl, setUser, setIsEditing, setError, setSuccessMessage, setErrorMessage, isAddressEditing, user, setAddress, setIsAddressEditing);


    const handleAddressEdit = () => {
        setIsAddressEditing(true);
    };

    const handleCancel = () => {
        setIsEditing(false);
        setIsAddressEditing(false);
        setFormData(prevState => {
            if (!prevState) return null;
            return {
                ...user,
                address: user?.addressId ? address : prevState.address
            } as User;
        });
        setCancelMessage("Operacja została przerwana.");
    };

    const {validatePhoneNumber, validateFirstName, validateBirthDate} = Validation();
    const handleInputChange = GetInputChangeHandle(validatePhoneNumber, setValidationErrors, validateBirthDate, validateFirstName, setFormData);


    return (
        <Paper style={{padding: '20px', maxWidth: '1600px', margin: '20px auto'}}>
            <Box display="flex" justifyContent="space-between">
                {/* Sekcja User's details */}
                {userRole ? GetUserDetails(user, error, formData, isEditing, handleInputChange, validationErrors, userRole as UserRoles, handleSave, handleCancel, handleEdit) : null}

                {GetUserAddress(address, isAddressEditing, countryList, formData, handleInputChange, handleAddressSave, handleCancel, handleAddressEdit)}
            </Box>
            {GetUserFormSnack(successMessage, setSuccessMessage, errorMessage, setErrorMessage, cancelMessage, setCancelMessage)}
        </Paper>
    );
};

export default UserForm;
