type UserContextType = {
    cookies: Record<string, string>;
    removePayloadCookie: () => void;
    isCookieSet: boolean;
    userRole?: string;
    avatarUrl?: string;
    userId?: number;
};

export default UserContextType;