import {createContext} from "react";
import UserContextType from "@/components/auth/userContextType";
import UserRoles from "@/enums/UserRoles";

const UserContext = createContext<UserContextType>({
    cookies: {},
    removePayloadCookie: () => {},
    isCookieSet: false,
    userRole: UserRoles.NON_LOGGED,
    avatarUrl: "",
    userId: undefined
});

export default UserContext;