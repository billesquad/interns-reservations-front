import {useEffect, useState} from "react";
import {useCookies} from "react-cookie";
import UserRoles from "@/enums/UserRoles";
import NavigationInterface from "@/navigation/NavigationInterface";
import fetchData from "@/fetching/fetchData";
import { GET_ROLE } from "@/fetching/urls/external/authenticationUrls";
import { CircularProgress, Container, Box } from '@mui/material';

const UserAuthInfo = () => {
    const [cookies, , removeCookie] = useCookies(["PAYLOAD"]);
    const [userId, setUserId] = useState<number | undefined>(undefined)
    const [userRole, setUserRole] = useState("");
    const [avatarUrl, setAvatarUrl] = useState("");
    const [isCookieSet, setIsCookieSet] = useState(false);
    const [isLoading, setIsLoading] = useState(true);

    const checkCookie = () => {
        setIsCookieSet(cookies.PAYLOAD === 'true');
    };

    const requestConfig = {
        partOfUrl: GET_ROLE
    };
    
    useEffect(() => {
        const initializeUserData = async () => {
            checkCookie();

            const response = await fetchData({requestConfig});
            if (response) {
                setUserRole(response.role);
                setUserId(response.userId);
                const payloadPicture = JSON.parse(atob(cookies.PAYLOAD)).picture;
                setAvatarUrl(payloadPicture);
            } else {
                setUserRole(UserRoles.NON_LOGGED);
            }
            setIsLoading(false);
        };

        initializeUserData();
    }, [cookies]);

    if (isLoading) {
        return (
            <Container>
                <Box 
                    display="flex" 
                    height="100vh" 
                    alignItems="center" 
                    justifyContent="center"
                >
                    <CircularProgress color="primary"/>
                </Box>
            </Container>
        );
    }

    return (
            <NavigationInterface
                cookies = {cookies}
                removePayloadCookie = {() => {removeCookie('PAYLOAD')}}
                isCookieSet = {isCookieSet}
                userRole = {userRole}
                avatarUrl = {avatarUrl}
                userId = {userId}
            />
    );
};

export default UserAuthInfo;
