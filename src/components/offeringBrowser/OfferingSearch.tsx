import React from 'react';
import { Box, Button, SelectChangeEvent, Grid, Typography } from '@mui/material';
import OfferingSearchParams from '@/entities/OfferingSearchParams';
import SearchIcon from '@mui/icons-material/Search';
import { gridSearchStyle, iconRightMarginStyle, inputSelectStyle, titleStyle } from '@/components/offeringBrowser/OfferingBrowserStyles';
import SearchInputField from '@/components/offeringBrowser/SearchInputField';
import SearchSelect from '@/components/offeringBrowser/SearchSelect';

interface OfferingSearchProps {
    onSearch: () => Promise<void>;
    params: OfferingSearchParams;
    handleInputChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
    handleSelectChange: (event: SelectChangeEvent) => void;
}

const OfferingSearch: React.FC<OfferingSearchProps> = ({ onSearch, params, handleInputChange, handleSelectChange }) => {
    const sortOptions = [
        { value: 'price', label: "Price" },
        { value: 'duration', label: "Duration" },
        { value: 'offeringName', label: "Service's name" },
        { value: 'provider', label: "Provider's specialization" },
        { value: 'createdAt', label: "Date of addition" },
        { value: 'modifiedAt', label: "Date of modification" },
    ];

    const directionOptions = [
        { value: 'ASC', label: 'Ascending' },
        { value: 'DESC', label: 'Descending' },
    ];

    return (
        <Box>
            <Typography style={titleStyle}>
                Search by
            </Typography>

            <Grid container spacing={2}>
                <Grid item xs={2}>
                    <SearchInputField
                        name="nameSearch"
                        label="Service's name"
                        value={params.nameSearch}
                        onChange={handleInputChange}
                    />
                </Grid>
                <Grid item xs={1.5}>
                    <SearchInputField
                        name="provider"
                        label="Provider's specialization"
                        value={params.provider}
                        onChange={handleInputChange}
                    />
                </Grid>
                <Grid item xs={1}>
                    <SearchInputField
                        type="number"
                        name="priceFrom"
                        label="Price from"
                        value={params.priceFrom}
                        onChange={handleInputChange}
                    />
                </Grid>
                <Grid item xs={1}>
                    <SearchInputField
                        type="number"
                        name="priceTo"
                        label="Price to"
                        value={params.priceTo}
                        onChange={handleInputChange}
                    />
                </Grid>
                <Grid item ml={64}>
                    <SearchSelect
                        name="sortParam"
                        label="Sort By"
                        value={params.sortParam}
                        options={sortOptions}
                        onChange={handleSelectChange}
                    />
                </Grid>
                <Grid item>
                    <SearchSelect
                        name="direction"
                        label="Sort Direction"
                        value={params.direction}
                        options={directionOptions}
                        onChange={handleSelectChange}
                    />
                </Grid>
                <Grid item>
                    <Box mt={2} style={gridSearchStyle}>
                        <Button
                            onClick={onSearch}
                            variant="contained"
                            color="primary"
                            sx={inputSelectStyle}
                        >
                            <SearchIcon style={iconRightMarginStyle} />
                            Search
                        </Button>
                    </Box>
                </Grid>

            </Grid>
        </Box>
    );
}

export default OfferingSearch;