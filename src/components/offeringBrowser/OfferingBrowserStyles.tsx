export const specializationStyle = { fontSize: '12px', marginRight: '20px', color: '#737373' };
export const textStyle = { fontSize: '16px', textDecoration: 'none', color: 'inherit' };
export const iconLeftMarginStyle = { fontSize: '16px', marginLeft: '4px' };
export const iconRightMarginStyle = { fontSize: '16px', marginRight: '4px' };
export const paperStyle = { padding: '16px', marginBottom: '16px', height: '200px', display: 'flex', justifyContent: 'space-between'};
export const titleStyle = { marginBottom: '10px', color: '#737373', fontSize: '20px' };
export const inputSelectStyle = { width: '120px', height: '40px' };
export const gridDescriptionStyle: React.CSSProperties = { justifyContent: 'space-between', width: '70%' };
export const gridDetailsStyle: React.CSSProperties = { justifyContent: 'space-between', width: '30%', alignItems: 'flex-end' };
export const gridSearchStyle: React.CSSProperties = { justifyContent: 'flex-end', display: 'flex' };

export const offeringNameStyle: React.CSSProperties = 
{ 
    whiteSpace: 'nowrap', 
    overflow: 'hidden', 
    textOverflow: 'ellipsis', 
    textDecoration: 'none', 
    color: 'inherit', 
    fontWeight: 'bold',
    fontSize: '24px' 
};
