import { useState, useEffect } from 'react';
import { Box, SelectChangeEvent } from '@mui/material';
import OfferingSearch from "@/components/offeringBrowser/OfferingSearch";
import OfferingResults from "@/components/offeringBrowser/OfferingResults";
import Offering from '@/entities/Offering';
import Page from '@/entities/Page';
import OfferingSearchParams from '@/entities/OfferingSearchParams';
import { fetchOfferings } from '@/components/offeringBrowser/OfferingBrowserService';

const OfferingBrowser = () => {
    const DEFAULT_PAGE = 0;
    const DEFAULT_ROWS_PER_PAGE = 10;

    const [offerings, setOfferings] = useState<Page<Offering>>();
    const [page, setPage] = useState(DEFAULT_PAGE);
    const [rowsPerPage, setRowsPerPage] = useState(DEFAULT_ROWS_PER_PAGE);
    const [params, setParams] = useState<OfferingSearchParams>({
        page: page,
        size: rowsPerPage,
        sortParam: 'price',
        direction: 'DESC',
        nameSearch: null,
        priceFrom: null,
        priceTo: null,
        provider: null
    });

    useEffect(() => {
        handleSubmit();
    }, [page, rowsPerPage, params.sortParam, params.direction]);

    const handleSubmit = async () => {
        try {
            const data = await fetchOfferings(params);
            setOfferings(data);
        } catch (error) {
            console.error('Error fetching offerings:', error);
        }
    };

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        setParams(prev => ({ ...prev, [name]: value === "" ? null : value }));
    };    

    const handleSelectChange = (event: SelectChangeEvent) => {
        const { name, value } = event.target;
        setParams(prev => ({ ...prev, [name]: value }));
    };

    const handleChangePage = (_event: unknown, newPage: number) => {
        setPage(newPage);
        setParams(prev => ({ ...prev, page: newPage }));
    };

    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        const newRowsPerPage = +event.target.value;
        setPage(DEFAULT_PAGE);
        setRowsPerPage(newRowsPerPage);
        setParams(prev => ({ ...prev, size: newRowsPerPage, page: DEFAULT_PAGE }));
    };

    return (
        <Box>
            <OfferingSearch
                onSearch={handleSubmit}
                params={params}
                handleInputChange={handleInputChange}
                handleSelectChange={handleSelectChange}
            />
            <OfferingResults
                offerings={offerings}
                page={page}
                rowsPerPage={rowsPerPage}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
            />
        </Box>
    );
}

export default OfferingBrowser;