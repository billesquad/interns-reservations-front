import { TextField } from "@mui/material";

interface SearchInputFieldProps {
    name: string;
    label: string;
    value: string | number | null;
    type?: string;
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const SearchInputField: React.FC<SearchInputFieldProps> = ({ name, label, value, type = 'text', onChange }) => (
    <TextField
        fullWidth
        type={type}
        name={name}
        label={label}
        variant="outlined"
        value={value || ''}
        onChange={onChange}
    />
);

export default SearchInputField;
