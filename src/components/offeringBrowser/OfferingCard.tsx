import React from 'react';
import { Grid, Typography, Paper, Button } from '@mui/material';
import { Link } from 'react-router-dom';
import PersonIcon from '@mui/icons-material/Person';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import SellIcon from '@mui/icons-material/Sell';
import BookmarkBorderIcon from '@mui/icons-material/BookmarkBorder';
import ClampLines from "react-clamp-lines";
import { formatDuration } from '@/components/utils/dateUtils';
import { OFFERING_RESERVE, PROVIDER_OVERVIEW } from '@/fetching/urls/internal/internalUrls';
import { gridDescriptionStyle, gridDetailsStyle, iconLeftMarginStyle, iconRightMarginStyle, offeringNameStyle, paperStyle, specializationStyle, textStyle } from '@/components/offeringBrowser/OfferingBrowserStyles';
import Offering from '@/entities/Offering';

interface OfferingCardProps {
    offering: Offering;
}

const OfferingCard: React.FC<OfferingCardProps> = ({ offering }) => {
    const offeringUrl = `${OFFERING_RESERVE}/${offering.offeringId}`;
    const providerUrl = `${PROVIDER_OVERVIEW}/${offering.providerId}`;

    return (
        <Paper elevation={3} style={paperStyle}>
            <Grid container direction="column" style={gridDescriptionStyle}>
                <Typography component={Link} to={offeringUrl} style={offeringNameStyle}>
                    {offering.offeringName}
                </Typography>

                {offering.description ? (
                    <ClampLines
                        text={offering.description}
                        id={offering.offeringId.toString()}
                        lines={2}
                        ellipsis="..."
                        buttons={false}
                        innerElement="p"
                    />
                ) : (
                    <Typography style={specializationStyle}>
                        Provider didn't add a description
                    </Typography>
                )}
            </Grid>

            <Grid container direction="column" style={gridDetailsStyle}>
                <Typography style={textStyle} component={Link} to={providerUrl}>
                    {offering.providerFullName}
                    <PersonIcon style={iconLeftMarginStyle} />
                </Typography>

                <Typography style={specializationStyle}>
                    {offering.providerSpecialization}
                </Typography>

                <Typography style={textStyle}>
                    {formatDuration(offering.duration)}
                    <AccessTimeIcon style={iconLeftMarginStyle} />
                </Typography>

                <Typography style={textStyle}>
                    {offering.price.toFixed(2)} {offering.currency}
                    <SellIcon style={iconLeftMarginStyle} />
                </Typography>

                <Link to={offeringUrl}>
                    <Button variant="contained" color="primary">
                        <BookmarkBorderIcon style={iconRightMarginStyle} />
                        Reserve
                    </Button>
                </Link>

            </Grid>
        </Paper>
    );
};

export default OfferingCard;
