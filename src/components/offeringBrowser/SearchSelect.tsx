import { inputSelectStyle, specializationStyle } from "@/components/offeringBrowser/OfferingBrowserStyles";
import { Grid, InputLabel, MenuItem, Select, SelectChangeEvent } from "@mui/material";

interface SearchSelectProps {
    name: string;
    label: string;
    value: string;
    options: { value: string; label: string }[];
    onChange: (event: SelectChangeEvent) => void;
}

const SearchSelect: React.FC<SearchSelectProps> = ({ name, label, value, options, onChange }) => (
    <Grid item>
        <InputLabel sx={specializationStyle}>{label}</InputLabel>
        <Select name={name} value={value} onChange={onChange} sx={inputSelectStyle}>
            {options.map((option) => (
                <MenuItem key={option.value} value={option.value}>
                    {option.label}
                </MenuItem>
            ))}
        </Select>
    </Grid>
);

export default SearchSelect;
