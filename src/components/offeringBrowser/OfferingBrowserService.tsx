import OfferingSearchParams from "@/entities/OfferingSearchParams";
import RequestTypes from "@/enums/RequestTypes";
import SendApiRequest from "@/fetching/SendApiRequest";
import { GET_OFFERINGS } from "@/fetching/urls/external/commonApiUrls";

const convertParamsToStrings = (params: OfferingSearchParams): Record<string, string> => {
    const result: Record<string, string> = {};
    for (const [key, value] of Object.entries(params)) {
        if (value !== null) {
            result[key] = String(value);
        }
    }
    return result;
};

export const fetchOfferings = async (params: OfferingSearchParams) => {
        return await SendApiRequest({
            partOfUrl: GET_OFFERINGS,
            typeOfRequest: RequestTypes.GET,
            urlParameters: convertParamsToStrings(params)
        });
}
