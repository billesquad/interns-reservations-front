import React from 'react';
import { Box, Typography, TablePagination } from '@mui/material';
import Offering from '@/entities/Offering';
import Page from '@/entities/Page';
import OfferingCard from '@/components/offeringBrowser/OfferingCard';
import { titleStyle } from '@/components/offeringBrowser/OfferingBrowserStyles';

interface OfferingResultsProps {
    offerings?: Page<Offering>;
    page: number;
    rowsPerPage: number;
    onPageChange: (event: unknown, newPage: number) => void;
    onRowsPerPageChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const OfferingResults: React.FC<OfferingResultsProps> = ({ offerings, page, rowsPerPage, onPageChange, onRowsPerPageChange }) => {
    return (
        <Box mt={2}>
            <Typography style={titleStyle}>
                Offerings
            </Typography>

            {offerings?.content.map(offering => (
                <OfferingCard key={offering.offeringId} offering={offering} />
            ))}

            <TablePagination
                component="div"
                count={offerings?.totalElements || 0}
                page={page}
                onPageChange={onPageChange}
                rowsPerPage={rowsPerPage}
                onRowsPerPageChange={onRowsPerPageChange}
            />
        </Box>
    );
};

export default OfferingResults;
