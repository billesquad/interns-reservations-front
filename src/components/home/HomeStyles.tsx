export const orangeBackgroundTextStyle = { backgroundColor: '#FF5900', padding: '0 5px', fontWeight: 'bold' };
export const imageStyle = {width: '200px' ,height: 'auto', marginTop: '5%' };
export const gridContainerStyle = { justifyContent: 'center', alignItems: 'center', minHeight: '60vh' };
export const boldStyle = { fontWeight: 'bold' };