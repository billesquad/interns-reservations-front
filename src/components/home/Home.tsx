import { Container, Typography, Grid } from "@mui/material";
import Logo from '@/assets/logo-r.svg';
import { orangeBackgroundTextStyle, imageStyle, boldStyle, gridContainerStyle } from "@/components/home/HomeStyles";

const Home = () => {
    return (
        <Container>
            <Grid container spacing={3} style={gridContainerStyle}>
                <Grid item xs={12}>
                    <Typography align="center" variant="h1" mt={'8%'}>
                        <span style={boldStyle}>Welcome</span> to the <span style={orangeBackgroundTextStyle}>Reservations</span>
                    </Typography>

                    <Typography align="center" variant="h4" paragraph mt={'3%'}>
                        Simple and quick booking of services in one place
                    </Typography>
                </Grid>
                <img src={Logo} alt="Logo" style={imageStyle} />
            </Grid>
        </Container>
    );
}

export default Home;
