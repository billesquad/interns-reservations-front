export const mainBoxStyle: React.CSSProperties = { display: "flex", flexDirection: "column", alignItems: "center", marginTop: '30px' };
export const innerBoxStyle = { width: '200px', height: "auto", marginTop: '40px' };
export const logoStyle = { width: '100%', height: 'auto', marginBottom: '30px' };
export const textBoxStyle: React.CSSProperties = { textAlign: 'center', marginBottom: '30px' };
export const welcomeTextStyle = { fontWeight: 'bold', fontSize: '36px' };
