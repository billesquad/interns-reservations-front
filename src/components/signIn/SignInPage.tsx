import { Button, Typography, Container, Box } from '@mui/material';
import { Google } from '@mui/icons-material';
import { AUTH_URL } from '@/fetching/urls/external/configUrls';
import Logo from '@/assets/logo-r.svg';
import { mainBoxStyle, innerBoxStyle, logoStyle, textBoxStyle, welcomeTextStyle } from '@/components/signIn/SignInPageStyles';

const SignInPage = () => {
    return (
        <Container>
            <Box style={mainBoxStyle}>
                <Box style={innerBoxStyle}>
                    <img src={Logo} alt="Logo" style={logoStyle} />
                </Box>
                <Box style={textBoxStyle}>
                    <Typography style={welcomeTextStyle}>
                        Welcome to the Reservations
                    </Typography>
                    <Typography>
                        This is the place to manage, track and book reservations.
                        Whether you're a business looking to manage reservations <br />
                        or an individual trying to schedule a service, we've got you covered.
                    </Typography>
                </Box>
                <Box>
                    <Button
                        variant="contained"
                        color="primary"
                        size="large"
                        startIcon={<Google />}
                        onClick={() => window.location.href = AUTH_URL}
                    >
                        Sign in with Google
                    </Button>
                </Box>
            </Box>
        </Container>
    );
}

export default SignInPage;
