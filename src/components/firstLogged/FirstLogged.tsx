import {Box, Button, IconButton, Link, Modal, Typography} from '@mui/material';
import LogoutIcon from '@mui/icons-material/Logout';
import {useContext, useState} from "react";
import RegistrationForm from "@/components/firstLogged/RegisterModal";
import {Errors} from "@/components/providerOfferings/providerOfferingsHelpers";
import {REGISTER_CONFIRMATION_MESSAGE, REGISTER_EMPTY_FORM} from "@/components/firstLogged/consts/registerValues";
import {orangeBackgroundTextStyle} from "@/components/home/HomeStyles";
import {ModalTypes} from "@/components/firstLogged/registerTypes";
import useUserContext from "@/components/auth/useUserContext";
import {useSnackbar} from "@/informationSnackbar/SnackbarProvider";
import ConfirmationModal from "@/universalComponents/ConfirmationModal";
import {closeModalType, logOutUser, sendRemoveUserRequest} from "@/components/firstLogged/FirstLogged.helpers";
import {registerStyleInnerBox, registerStyleOuterBox} from "@/components/firstLogged/consts/styles";

const RegistrationPage = () => {
    const [openModalType, setOpenModalType] = useState("");
    const [openModalLogout, setOpenModalLogout] = useState(false);

    const [errors, setErrors] = useState<Errors>({});
    const [registerData, setRegisterData] = useState(REGISTER_EMPTY_FORM);
    const {userId, removePayloadCookie} = useContext(useUserContext);
    const {openSnackbar} = useSnackbar();

    const handleOpenModal = (type: ModalTypes) => setOpenModalType(type);
    const handleCloseModal = closeModalType(setOpenModalType, setErrors, setRegisterData);
    const handleOpenModalLogout = () => {
        setOpenModalLogout(true);
    };
    const handleCloseModalLogout = () => {
        setOpenModalLogout(false);
    };

    const handleRemoveUserRequest = sendRemoveUserRequest(openSnackbar, removePayloadCookie);
    const handleLogout = logOutUser(handleRemoveUserRequest, userId, handleCloseModalLogout);

    return (
        <Box>
            <Box sx={registerStyleOuterBox}>
                {userId && (
                    <IconButton onClick={handleOpenModalLogout} title="Logout" color="primary">
                        <LogoutIcon/>
                    </IconButton>
                )}
            </Box>
            <Box sx={registerStyleInnerBox}>
                <Typography variant="h1" gutterBottom>
                    <span style={orangeBackgroundTextStyle}>Register</span><span> Your Account </span>
                </Typography>

                <Box>
                    <Typography variant="h3" paragraph>
                        To access the full features of our platform, you need to register an account.
                    </Typography>
                    <ul>
                        <Typography align="center" paragraph variant="body2">
                            <strong>Registering as a Customer:</strong> Enjoy a personalized experience, get
                            reservations, and more!
                        </Typography>
                        <Typography align="center" paragraph variant="body2" mt={2}>
                            <strong>Registering as a Provider:</strong> Offer your services, connect with customers,
                            and grow your business!
                        </Typography>
                    </ul>
                </Box>
                <Button variant="contained" color="primary" onClick={() => handleOpenModal('customerModal')}>
                    Register as a Customer
                </Button>
                <Link component="button" variant="body2" onClick={() => handleOpenModal('providerModal')}
                      color="textSecondary">
                    Register as a Provider
                </Link>
                <Modal open={openModalType === 'customerModal'} onClose={handleCloseModal}
                       aria-labelledby="customer-registration">
                    <RegistrationForm modalSubject="customerModal" errors={errors} setErrors={setErrors}
                                      registerData={registerData} setRegisterData={setRegisterData}/>
                </Modal>
                <Modal open={openModalType === 'providerModal'} onClose={handleCloseModal}
                       aria-labelledby="provider-registration">
                    <RegistrationForm modalSubject="providerModal" errors={errors} setErrors={setErrors}
                                      registerData={registerData} setRegisterData={setRegisterData}/>
                </Modal>
                <ConfirmationModal open={openModalLogout} onClose={handleCloseModalLogout}
                                   message={REGISTER_CONFIRMATION_MESSAGE} onConfirm={handleLogout}/>

            </Box>
        </Box>
    );
}

export default RegistrationPage;
