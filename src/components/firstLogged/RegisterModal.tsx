import {Button, Typography, TextField, Box, CircularProgress, Fade, Backdrop} from '@mui/material';
import React, {ForwardRefRenderFunction, useState} from "react";
import {useSnackbar} from "@/informationSnackbar/SnackbarProvider";
import ObjectData from "@/universalComponents/ObjectDataType";
import {Errors} from "@/components/providerOfferings/providerOfferingsHelpers";
import submitData from "@/components/firstLogged/utils/submitData";
import validateRegistrationForm from "@/components/firstLogged/utils/validateRegistrationForm";
import {ModalTypes} from "@/components/firstLogged/registerTypes";
import {registerModalStyleBox} from "@/components/firstLogged/consts/styles";


interface ModalProp {
    modalSubject: ModalTypes;
    registerData: ObjectData
    setRegisterData: React.Dispatch<React.SetStateAction<ObjectData>>;
    errors: Errors;
    setErrors: (errors: Errors) => void;
}

const RegistrationForm: ForwardRefRenderFunction<HTMLDivElement, ModalProp> = ({
                                                                                   modalSubject,
                                                                                   registerData,
                                                                                   setRegisterData,
                                                                                   errors,
                                                                                   setErrors
                                                                               }, ref) => {
    const [isSubmitting, setIsSubmitting] = useState(false);
    const {openSnackbar} = useSnackbar();

    const validate = validateRegistrationForm(registerData, modalSubject ,setErrors);
    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const {name, value} = event.target;
        setRegisterData((prevState) => ({...(prevState as ObjectData), [name]: value}));
    };
    const handleSubmit = submitData(validate, setIsSubmitting, modalSubject, registerData, openSnackbar);

    return (
        <>
            <Box tabIndex={0} ref={ref} sx={registerModalStyleBox}>
                <Typography variant="h6"
                            mb={2}>{(modalSubject == 'providerModal') ? "Provider" : "Customer"} Registration</Typography>
                <TextField onChange={handleInputChange} value={registerData.firstName} name="firstName"
                           label="First Name"
                           variant="outlined" fullWidth
                           margin="normal"
                           error={!!errors.firstName}
                           helperText={errors.firstName}/>
                <TextField onChange={handleInputChange} value={registerData.lastName} name="lastName" label="Last Name"
                           variant="outlined" fullWidth
                           margin="normal"
                           error={!!errors.lastName}
                           helperText={errors.lastName}/>
                <TextField onChange={handleInputChange} value={registerData.birthDate} name="birthDate"
                           label="Birth Date"
                           variant="outlined" fullWidth
                           margin="normal"
                           type="date" InputLabelProps={{shrink: true}}
                           error={!!errors.birthDate}
                           helperText={errors.birthDate}/>
                <TextField onChange={handleInputChange} value={registerData.phoneNumber} name="phoneNumber"
                           label="Phone Number" variant="outlined" fullWidth
                           margin="normal"
                           error={!!errors.phoneNumber}
                           helperText={errors.phoneNumber}/>
                {modalSubject === 'providerModal' &&
                    <TextField onChange={handleInputChange} value={registerData.specialization} name="specialization"
                               label="Specialization" variant="outlined" fullWidth
                               margin="normal"
                               error={!!errors.specialization}
                               helperText={errors.specialization}/>}
                <Button onClick={handleSubmit} variant="contained" color="primary" fullWidth
                        sx={{mt: 2}}>Submit</Button>
            </Box>
            <Fade in={isSubmitting}>
                <Backdrop
                    sx={{color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1}}
                    open={isSubmitting}
                >
                    <CircularProgress color="primary"/>
                </Backdrop>
            </Fade>
        </>
    )
};

const NamedRegistrationForm = React.forwardRef(RegistrationForm);
export default NamedRegistrationForm;