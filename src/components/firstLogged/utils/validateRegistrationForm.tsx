import ObjectData from "@/universalComponents/ObjectDataType";
import {Errors} from "@/components/providerOfferings/providerOfferingsHelpers";

const validateRegistrationForm = (registerData: ObjectData, modalSubject: string,setErrors: (errors: Errors) => void) => {
    return (): boolean => {
        const tempErrors: Errors = {};

        tempErrors.firstName =
            registerData.firstName?.trim().length >= 2 && /^[a-zA-Z]+$/.test(registerData.firstName)
                ? ""
                : "First name must be between 2 and 255 characters and consist only of letters";

        tempErrors.lastName =
            registerData.lastName?.trim().length >= 2 && /^[a-zA-Z]+$/.test(registerData.lastName)
                ? ""
                : "Last name must be between 2 and 255 characters and consist only of letters";

        const birthDate = new Date(registerData.birthDate);
        const adultDate = new Date();
        adultDate.setFullYear(adultDate.getFullYear() - 18); // assuming an adult is 18 years or older
        tempErrors.birthDate = birthDate <= adultDate ? "" : "User must be an Adult";

        tempErrors.phoneNumber = registerData.phoneNumber && /^\d{7,15}$/.test(registerData.phoneNumber)
            ? ""
            : "Phone number must be between 7 and 15 digits";

        tempErrors.specialization = (modalSubject == "customerModal" || (registerData.specialization?.trim().length > 0) ? "" : "Specialization must not be empty");

        setErrors(tempErrors);

        return Object.values(tempErrors).every(err => err === "");
    };
}

export default validateRegistrationForm;