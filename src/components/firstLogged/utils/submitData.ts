import ObjectData from "@/universalComponents/ObjectDataType";
import RequestTypes from "@/enums/RequestTypes";
import SendApiRequest from "@/fetching/SendApiRequest";

const submitData = (validate: () => boolean,
                    setIsSubmitting: (value: (((prevState: boolean) => boolean) | boolean)) => void,
                    modalSubject: "providerModal" | "customerModal",
                    registerData: ObjectData,
                    openSnackbar: (message: string, severity: ("success" | "error" | "info" | "warning")) => void
) => {
    return () => {
        if (validate()) {
            setIsSubmitting(true);
            const fetchData = async () => {
                const requestConfig = {
                    partOfUrl: `/register/${modalSubject === 'providerModal' ? 'provider' : "customer"}`,
                    typeOfRequest: RequestTypes.POST,
                    body: {
                        "firstName": registerData.firstName,
                        "lastName": registerData.lastName,
                        "birthDate": registerData.birthDate,
                        "phoneNumber": registerData.phoneNumber,
                        ...((modalSubject === 'providerModal') && {"specialization": registerData.specialization})
                    }
                };
                try {
                    return await SendApiRequest(requestConfig, openSnackbar).then(() => {
                        window.location.reload()
                    });
                } catch (error) {
                    setIsSubmitting(false);
                    return null;
                }
            };
            fetchData();
        }
    };
}

export default submitData;