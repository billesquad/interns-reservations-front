import ObjectData from "@/universalComponents/ObjectDataType";

export const REGISTER_EMPTY_FORM: ObjectData = {
    firstName: "",
    lastName: "",
    birthDate: "",
    phoneNumber: "",
    specialization: "",
}

export const REGISTER_CONFIRMATION_MESSAGE = "Log out from current session?"
