export const registerStyleInnerBox = {display: 'flex', flexDirection: 'column', alignItems: 'center', gap: 3, mt: 10}
export const registerStyleOuterBox = {display: 'flex', justifyContent: 'flex-end'}
export const registerModalStyleBox = {
    width: 400,
    padding: 3,
    bgcolor: 'background.paper',
    margin: 'auto',
    mt: 10,
    border: '1px solid #e0e0e0',
    borderRadius: 2
}