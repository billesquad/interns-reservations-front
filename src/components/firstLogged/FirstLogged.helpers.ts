import SendApiRequest from "@/fetching/SendApiRequest";
import {REMOVE_USER} from "@/fetching/urls/external/authenticationUrls";
import {Errors} from "@/components/providerOfferings/providerOfferingsHelpers";
import {REGISTER_EMPTY_FORM} from "@/components/firstLogged/consts/registerValues";
import ObjectData from "@/universalComponents/ObjectDataType";

export const sendRemoveUserRequest = (openSnackbar: (message: string, severity: ("success" | "error" | "info" | "warning")) => void, removePayloadCookie: () => void) => {
    return (userId: string) => {
        SendApiRequest({
            partOfUrl: REMOVE_USER(userId), typeOfRequest: "DELETE"
        }, openSnackbar).then(removePayloadCookie);
    };
}
export const logOutUser = (handleRemoveUserRequest: (userId: string) => void, userId: number | undefined, handleCloseModalLogout: () => void) => {
    return () => {
        handleRemoveUserRequest((userId as number).toString());
        handleCloseModalLogout();
    };
}

export const closeModalType = (setOpenModalType: (value: (((prevState: string) => string) | string)) => void, setErrors: (value: (((prevState: Errors) => Errors) | Errors)) => void, setRegisterData: (value: (((prevState: ObjectData) => ObjectData) | ObjectData)) => void) => {
    return () => {
        setOpenModalType("");
        setErrors({});
        setRegisterData(REGISTER_EMPTY_FORM);
    };
}