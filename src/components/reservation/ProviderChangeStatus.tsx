import React, { useState } from "react";
import { Box, Button, Select, MenuItem, Typography } from "@mui/material";
import ConfirmationDialog from "@/components/reservation/ConfirmationDialog";
import { useSnackbar } from '@/informationSnackbar/SnackbarProvider';
import { warnTextStyle, selectStyle, changeStatusButtonStyle, boxStyle } from "@/components/reservation/ReservationDetailsStyles";
import { changeReservationStatusById } from "@/components/reservation/ReservationDetailsService";
import { formatReservationStatus, isCancelled } from "@/components/utils/reservationStatusUtils";

interface ProviderChangeStatusProps {
    reservationId: number;
    status: string;
    onUpdateStatus: (newStatus: string) => void;
}

const ProviderChangeStatus: React.FC<ProviderChangeStatusProps> = ({ reservationId, status, onUpdateStatus }) => {
    const [openDialog, setOpenDialog] = useState(false);
    const { openSnackbar } = useSnackbar();
    const [dialogMessage, setDialogMessage] = useState("");
    const [selectedStatus, setSelectedStatus] = useState<string>(status);

    const confirmChanging = () => {
        setDialogMessage(`Are you sure you want to change the status from ${formatReservationStatus(status)} to ${formatReservationStatus(selectedStatus)}?`);
        setOpenDialog(true);
    }

    const handleChangeStatus = async () => {
        try {
            const updatedReservation = await changeReservationStatusById(reservationId.toString(), selectedStatus);
            setOpenDialog(false);
            openSnackbar('Successfully changed status.', 'success');
            onUpdateStatus(updatedReservation.status);
            setSelectedStatus(selectedStatus);
        } catch (error) {
            console.error('Error changing the reservation status:', error);
        }
    };

    return (
        <Box sx={boxStyle}>
            <Select
                value={selectedStatus}
                onChange={(event) => setSelectedStatus(event.target.value)}
                displayEmpty
                sx={selectStyle}
                disabled={isCancelled(status)}
            >
                <MenuItem value="AWAITING_FOR_APPROVAL">Await for Approval</MenuItem>
                <MenuItem value="ACCEPTED">Accept</MenuItem>
                <MenuItem value="DECLINED">Decline</MenuItem>
                <MenuItem value="DONE">Done</MenuItem>
                {status === "CANCELLED" && (
                    <MenuItem value="CANCELLED" disabled>Cancelled</MenuItem>
                )}
            </Select>

            <Button
                onClick={confirmChanging}
                variant="contained"
                color="primary"
                sx={changeStatusButtonStyle}
                disabled={selectedStatus === status}
            >
                Change status
            </Button>

            {isCancelled(status) && (
                <Typography style={warnTextStyle}>
                    The status of a canceled reservation cannot be changed
                </Typography>
            )}
            {(status === selectedStatus && !isCancelled(status)) && (
                <Typography style={warnTextStyle}>
                    The reservation already has status {formatReservationStatus(status)}
                </Typography>
            )}
            
            <ConfirmationDialog
                open={openDialog}
                onClose={() => setOpenDialog(false)}
                onConfirm={handleChangeStatus}
                message={dialogMessage}
            />
        </Box>
    );
};

export default ProviderChangeStatus;
