import SendApiRequest from '@/fetching/SendApiRequest';
import RequestTypes from '@/enums/RequestTypes';
import { CANCEL_RESERVATION } from '@/fetching/urls/external/customerApiUrls';
import { GET_RESERVATION_BY_ID } from '@/fetching/urls/external/commonApiUrls';
import { CHANGE_RESERVATION_STATUS } from '@/fetching/urls/external/providerApiUrls';

export const fetchReservationById = async (reservationId: string) => {
    return await SendApiRequest({
        partOfUrl: GET_RESERVATION_BY_ID(reservationId),
        typeOfRequest: RequestTypes.GET
    });
}

export const cancelReservationById = async (reservationId: string) => {
    return await SendApiRequest({
        partOfUrl: CANCEL_RESERVATION(reservationId),
        typeOfRequest: RequestTypes.PATCH
    });
}

export const changeReservationStatusById = async (reservationId: string, status: string) => {
    return await SendApiRequest({
        partOfUrl: CHANGE_RESERVATION_STATUS(reservationId),
        typeOfRequest: RequestTypes.PATCH,
        urlParameters: {
            status: status
        }
    });
}
