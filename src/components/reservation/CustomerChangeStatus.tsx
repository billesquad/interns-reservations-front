import React, { useState } from "react";
import { Box, Button, Typography } from "@mui/material";
import { canBeCancelled } from "@/components/utils/reservationStatusUtils";
import ConfirmationDialog from "@/components/reservation/ConfirmationDialog";
import { useSnackbar } from '@/informationSnackbar/SnackbarProvider';
import { boxStyle, warnTextStyle } from "@/components/reservation/ReservationDetailsStyles";
import { cancelReservationById } from "@/components/reservation/ReservationDetailsService";
import { isLessThanOneDayBeforeStart } from "@/components/utils/dateUtils";

interface CustomerChangeStatusProps {
    startDate: string;
    reservationId: number;
    status: string;
    onUpdateStatus: (newStatus: string) => void;
}

const CustomerChangeStatus: React.FC<CustomerChangeStatusProps> = ({ startDate, reservationId, status, onUpdateStatus }) => {
    const [openDialog, setOpenDialog] = useState(false);
    const { openSnackbar } = useSnackbar();

    const disableButton = isLessThanOneDayBeforeStart(startDate) || !canBeCancelled(status);

    const handleCancelReservation = async () => {
        try {
            const updatedReservation = await cancelReservationById(reservationId.toString());
            setOpenDialog(false);
            openSnackbar('Successfully cancelled.', 'success');
            onUpdateStatus(updatedReservation.status);
        } catch (error) {
            console.error('Error cancelling the reservation:', error);
        }
    }

    const confirmCancellation = () => {
        setOpenDialog(true);
    }

    return (
        <Box style={boxStyle}>
            <Button variant="contained" color="primary" onClick={confirmCancellation} disabled={disableButton}>
                Cancel reservation
            </Button>
            
            <Typography style={warnTextStyle}>
                Reservation cannot be canceled less than a day before start time
            </Typography>

            <ConfirmationDialog
                open={openDialog}
                onClose={() => setOpenDialog(false)}
                onConfirm={handleCancelReservation}
                message="Are you sure you want to cancel the reservation?"
            />
        </Box>
    );
};

export default CustomerChangeStatus;
