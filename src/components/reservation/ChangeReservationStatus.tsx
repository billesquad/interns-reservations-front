import React from "react";
import UserRoles from "@/enums/UserRoles";
import CustomerChangeStatus from "@/components/reservation/CustomerChangeStatus";
import ProviderChangeStatus from "@/components/reservation/ProviderChangeStatus";

interface ChangeReservationStatusProps {
    startDate: string;
    reservationId: number;
    status: string;
    onUpdateStatus: (newStatus: string) => void;
    userRole: string;
}

const ChangeReservationStatus: React.FC<ChangeReservationStatusProps> = ({ startDate, reservationId, status, onUpdateStatus, userRole }) => {
    if (userRole === UserRoles.ROLE_CUSTOMER) {
        return <CustomerChangeStatus
            startDate={startDate}
            reservationId={reservationId}
            status={status}
            onUpdateStatus={onUpdateStatus}
        />;
    }

    if (userRole === UserRoles.ROLE_PROVIDER) {
        return <ProviderChangeStatus
            reservationId={reservationId}
            status={status}
            onUpdateStatus={onUpdateStatus}
        />;
    }
};

export default ChangeReservationStatus;
