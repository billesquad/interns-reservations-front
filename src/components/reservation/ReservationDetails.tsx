import { fetchReservationById } from "@/components/reservation/ReservationDetailsService";
import UserContext from "@/components/auth/useUserContext";
import Reservation from "@/entities/Reservation";
import { Box, CircularProgress, Grid, Tooltip, Typography } from "@mui/material";
import { useContext, useEffect, useState } from "react";
import { Link, useParams } from 'react-router-dom';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import SellIcon from '@mui/icons-material/Sell';
import PersonIcon from '@mui/icons-material/Person';
import FlakyIcon from '@mui/icons-material/Flaky';
import { formatDateToCustomFormat, getTime } from "@/components/utils/dateUtils";
import { formatReservationStatus } from "@/components/utils/reservationStatusUtils";
import ChangeReservationStatus from "@/components/reservation/ChangeReservationStatus";
import UserRoles from "@/enums/UserRoles";
import { alignCenter, boxProgressStyle, descriptionStyle, gridStyle, iconStyle, marginRightStyle, nullDescriptionStyle, offeringNameStyle, specializationStyle, textStyle } from "@/components/reservation/ReservationDetailsStyles";
import { CUSTOMER_OVERVIEW, OFFERING_RESERVE, PROFILE, PROVIDER_OVERVIEW } from "@/fetching/urls/internal/internalUrls";

const ReservationDetails = () => {
    const { userRole } = useContext(UserContext);
    const { id } = useParams<{ id: string }>();
    const [reservation, setReservation] = useState<Reservation | null>(null);
    const [status, setStatus] = useState<string | null>(reservation?.status || null);

    const offeringUrl = `${OFFERING_RESERVE}/${reservation?.offeringId}`;
    const providerUrl = `${PROVIDER_OVERVIEW}/${reservation?.providerId}`;
    const customerUrl = `${CUSTOMER_OVERVIEW}/${reservation?.customerId}`;

    useEffect(() => {
        const fetchData = async () => {
            try {
                const data = await fetchReservationById(id!);
                setReservation(data);
                setStatus(data.status);
            } catch (error) {
                console.error('Error fetching reservation:', error);
            }
        };
        fetchData();
    }, [id]);

    const formattedEndDate = reservation ? getTime(new Date(reservation.reservationDateEnd)) : "";

    if (!reservation) {
        return (
            <Box style={boxProgressStyle}>
                <CircularProgress />
            </Box>
        );
    }

    return (
        <Box>
            <Grid container>
                <Grid item xs={6}>
                    {userRole === UserRoles.ROLE_CUSTOMER ? (
                        <Typography style={offeringNameStyle} component={Link} to={offeringUrl}>
                            {reservation.offeringName}
                        </Typography>
                    ) : (
                        <Typography style={offeringNameStyle}>
                            {reservation.offeringName}
                        </Typography>
                    )}

                    {reservation.description !== null ? (
                        <Typography
                            style={descriptionStyle}>
                            {reservation.description}
                        </Typography>
                    ) : (
                        <Typography
                            style={nullDescriptionStyle}>
                            Provider has not added a description
                        </Typography>
                    )}

                    {userRole === UserRoles.ROLE_PROVIDER ? (
                        <Typography style={iconStyle} component={Link} to={PROFILE}>
                            <PersonIcon style={iconStyle} />
                            {reservation.providerFullName}
                        </Typography>
                    ) : (
                        <Typography style={iconStyle} component={Link} to={providerUrl}>
                            <PersonIcon style={iconStyle} />
                            {reservation.providerFullName}
                        </Typography>
                    )}
                    <Typography style={specializationStyle}>
                        {reservation.providerSpecialization}
                    </Typography>
                </Grid>
                <Grid item xs={6}>
                    <Grid container direction="column" sx={gridStyle}>
                        <Grid item>
                            <Typography style={textStyle}>
                                <CalendarMonthIcon style={marginRightStyle} />
                                Date of reservation: {formatDateToCustomFormat(reservation.reservationDate)}
                            </Typography>

                            <Typography style={textStyle}>
                                <AccessTimeIcon style={marginRightStyle} />
                                Expected completion time: {formattedEndDate}
                            </Typography>

                            {userRole === UserRoles.ROLE_PROVIDER && (
                                <Tooltip title={reservation.customerFullName} placement="bottom">
                                    <Typography style={textStyle} component={Link} to={customerUrl}>
                                        <PersonIcon style={marginRightStyle} />
                                        Customer: {truncateText(reservation.customerFullName, 30)}
                                    </Typography>
                                </Tooltip>
                            )}

                            <Typography style={textStyle}>
                                <SellIcon style={marginRightStyle} />
                                Price: {reservation.price.toFixed(2)} {reservation.currency}
                            </Typography>
                            <Typography style={textStyle}>
                                <FlakyIcon style={marginRightStyle} />
                                Status: {formatReservationStatus(status)}
                            </Typography>
                        </Grid>
                        <Grid item style={alignCenter}>
                            <ChangeReservationStatus
                                startDate={reservation.reservationDate}
                                reservationId={reservation.reservationId}
                                status={status!}
                                onUpdateStatus={setStatus}
                                userRole={userRole!} />
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Box>
    )
}

export default ReservationDetails;

const truncateText = (text: string, maxLength: number) => {
    if (text.length > maxLength) {
        return text.substring(0, maxLength) + '...';
    }
    return text;
};
