export const offeringNameStyle = { marginBottom: '16px', fontSize: '28px', fontWeight: 'bold', textDecoration: 'none', color: 'inherit' };
export const descriptionStyle: React.CSSProperties = { marginBottom: '20px', fontSize: '16px', textAlign: 'justify' };
export const nullDescriptionStyle = { marginBottom: '20px', fontSize: '12px', color: '#737373' };
export const iconStyle = { fontSize: '16px', color: '#737373', marginRight: '4px', textDecoration: 'none' };
export const specializationStyle = { fontSize: '12px', color: '#858585', marginLeft: '20px' };
export const textStyle = { fontSize: '16px', marginBottom: '4px', textDecoration: 'none', color: 'inherit' };
export const marginRightStyle = { marginRight: '6px' };
export const alignCenter = { alignSelf: 'center' };
export const gridStyle = { alignItems: 'center', justifyContent: 'flex-start', marginTop: '180px' };
export const dialogActionsStyle = { justifyContent: 'space-between', padding: '20px 32px' };
export const dialogIconsStyle = { marginRight: '8px', fontSize: '20px' };
export const warnTextStyle = { fontSize: '12px', color: '#737373', marginTop: '10px' };
export const boxStyle: React.CSSProperties = { display: 'flex', flexDirection: 'column', alignItems: 'center', marginTop: '100px' };
export const selectStyle = { width: '200px', marginBottom: '15px' };
export const changeStatusButtonStyle = { height: '40px', width: '160px' };
export const boxProgressStyle = { display: 'flex', justifyContent: 'center', alignItems: 'center', minHeight: '70vh' };
