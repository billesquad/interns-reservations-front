import {
    Avatar,
    Box,
    Divider,
    List,
    ListItem,
    ListItemAvatar,
    ListItemButton,
    ListItemText,
    Paper,
    Typography
} from "@mui/material";
import {useContext, useState} from "react";
import useUserContext from "@/components/auth/useUserContext";
import ObjectData from "@/universalComponents/ObjectDataType";
import EditWorkHourModal from "@/components/providerCalendar/EditWorkHourModal";
import {Errors} from "@/components/providerOfferings/providerOfferingsHelpers";
import {useSnackbar} from "@/informationSnackbar/SnackbarProvider";
import {saveWorkHours} from "@/components/providerCalendar/utils/saveWorkHours";
import {
    providerCalendarStyleDescription,
    providerCalendarStyleMainBox, providerCalendarStyleMainList
} from "@/components/providerCalendar/providerCalendarStyles";
import {removeWorkHourSeconds} from "@/components/providerCalendar/providerCalendarHelper";
import useFetchCalendarData from "@/components/providerCalendar/utils/fetchData";
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';

const ProviderCalendar = () => {
    const {userId} = useContext(useUserContext);
    const {openSnackbar} = useSnackbar();

    const [forceFetch, setForceFetch] = useState<boolean>(true);
    const [fetchedData, setFetchedData] = useState<ObjectData[]>();
    const [editModalOpen, setEditModalOpen] = useState(false);
    const [selectedWorkHour, setSelectedWorkHour] = useState<string | null>(null);
    const [errors, setErrors] = useState<Errors>({});
    const [startTime, setStartTime] = useState<string>("");
    const [endTime, setEndTime] = useState<string>("");
    useFetchCalendarData(userId, forceFetch, setFetchedData);

    const handleListItemClick = (workHour: ObjectData) => {
        setSelectedWorkHour(workHour.workHourId);
        setStartTime(workHour.startTime ? removeWorkHourSeconds(workHour.startTime) : '');
        setEndTime(workHour.endTime ? removeWorkHourSeconds(workHour.endTime) : '');
        setEditModalOpen(true);
    };

    const handleModalClose = () => {
        setErrors({})
        setEditModalOpen(false);
        setSelectedWorkHour(null);
    };

    const handleSave = saveWorkHours(startTime, endTime, selectedWorkHour, setStartTime, setEndTime, openSnackbar, setForceFetch, forceFetch);
    const handleRemove = saveWorkHours("", "", selectedWorkHour, setStartTime, setEndTime, openSnackbar, setForceFetch, forceFetch);

    return (
        <Box sx={providerCalendarStyleMainBox}>
            <Typography mb='1rem' sx={{fontWeight: 'bold', fontSize: '32px'}}>
                Your Work Hours
            </Typography>
            <Typography sx={providerCalendarStyleDescription}>
                Complete it - on this basis, customers will reserve time for your offerings
            </Typography>
            <Box width="30rem">
                <Paper square={false} elevation={3}>
                    {fetchedData &&
                        <List sx={providerCalendarStyleMainList}>
                            <ListItem>
                                <ListItemAvatar>
                                    <Avatar>
                                        <CalendarMonthIcon/>
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary="Work hours" secondary="Click day to change your work hours"/>
                            </ListItem>
                            <Divider/>
                            {(fetchedData as ObjectData[]).map((workHourData, index) => {
                                const labelId = `checkbox-list-label-${index}`;
                                const openHours = workHourData.startTime && workHourData.endTime
                                    ? `${removeWorkHourSeconds(workHourData.startTime)} - ${removeWorkHourSeconds(workHourData.endTime)}`
                                    : "Closed";
                                return (
                                    <>
                                        <ListItem
                                            key={index}
                                            disablePadding
                                        >
                                            <ListItemButton role={undefined}
                                                            onClick={() => handleListItemClick(workHourData)}
                                                            sx={{width: '50vw'}}>
                                                <ListItemText id={labelId} primary={workHourData.dayOfWeek}/>
                                                <ListItemText id={labelId} primary={openHours} sx={{textAlign: 'end'}}/>
                                            </ListItemButton>
                                        </ListItem>
                                        <Divider/>
                                    </>
                                );
                            })}
                        </List>
                    }
                </Paper>
            </Box>
            {selectedWorkHour && (
                <EditWorkHourModal
                    open={editModalOpen}
                    onClose={handleModalClose}
                    workHourId={selectedWorkHour}
                    onSave={handleSave}
                    onRemove={handleRemove}
                    errors={errors}
                    setErrors={setErrors}
                    startTime={startTime}
                    setStartTime={setStartTime}
                    endTime={endTime}
                    setEndTime={setEndTime}
                />
            )}
        </Box>
    )
}

export default ProviderCalendar;