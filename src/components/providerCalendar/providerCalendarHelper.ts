export const removeWorkHourSeconds = (workHour: string) => {
    return workHour.slice(0, -3);
}