import {DELETE_WORK_HOUR, UPDATE_WORK_HOUR} from "@/fetching/urls/external/providerApiUrls";
import RequestTypes from "@/enums/RequestTypes";
import SendApiRequest from "@/fetching/SendApiRequest";

export const saveWorkHours = (startTime: string, endTime: string, selectedWorkHour: string | null, setStartTime: (value: (((prevState: string) => string) | string)) => void, setEndTime: (value: (((prevState: string) => string) | string)) => void, openSnackbar: (message: string, severity: ("success" | "error" | "info" | "warning")) => void, setForceFetch: (value: (((prevState: boolean) => boolean) | boolean)) => void, forceFetch: boolean) => {
    return async () => {

        const isDeleteRequest = startTime === "" && endTime === "";
        const partOfUrl = isDeleteRequest ? DELETE_WORK_HOUR(selectedWorkHour as string) : UPDATE_WORK_HOUR(selectedWorkHour as string);

        const requestConfig = isDeleteRequest
            ? {partOfUrl, typeOfRequest: RequestTypes.PATCH}
            : {
                partOfUrl,
                typeOfRequest: RequestTypes.PATCH,
                body: {
                    "startTime": startTime,
                    "endTime": endTime,
                }
            };

        try {
            setStartTime(startTime ? startTime : '');
            setEndTime(endTime ? endTime : '');
            await SendApiRequest(requestConfig, openSnackbar).then(() => setForceFetch(!forceFetch));
        } catch (error) {
            console.error("Error fetching reservation data:", error);
        }
    };
}