import ObjectData from "@/universalComponents/ObjectDataType";
import {useEffect} from "react";
import {GET_PROVIDER_CALENDAR} from "@/fetching/urls/external/commonApiUrls";
import SendApiRequest from "@/fetching/SendApiRequest";

const useFetchCalendarData = (userId: number | undefined, forceFetch: boolean, setFetchedData: (value: (((prevState: (ObjectData[] | undefined)) => (ObjectData[] | undefined)) | ObjectData[] | undefined)) => void) => {
    useEffect(() => {
        if (!userId) return;
        fetchData();
    }, [forceFetch]);
    const fetchData = async () => {
        try {
            const partOfUrl = GET_PROVIDER_CALENDAR((userId as number).toString())
            const data = await SendApiRequest({
                partOfUrl,
            });
            setFetchedData(data)
        } catch (error) {
            console.error("Error fetching reservation data:", error);
        }
    }
}

export default useFetchCalendarData;