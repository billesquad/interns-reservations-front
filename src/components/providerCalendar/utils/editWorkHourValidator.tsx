import {Errors} from "@/components/providerOfferings/providerOfferingsHelpers";
import convertTimeToMinutes from "@/components/utils/convertTimeToMinutes";

const editWorkHourValidator = (startTime: string, endTime: string, setErrors: (value: (((prevState: Errors) => Errors) | Errors)) => void) => {
    return (): boolean => {
        const tempErrors: Errors = {};
        tempErrors.emptyFields =
            ((startTime === "") && (endTime !== "")) || ((startTime !== "") && (endTime === ""))
                ? "Both fields must be empty or filled."
                : "";
        if ((startTime !== "") && (endTime !== "")) {
            tempErrors.atLeastOneHour =
                (convertTimeToMinutes(endTime) - convertTimeToMinutes(startTime) > 60)
                    ? ""
                    : "There must be at least 1 hour between the start time and the end time.";
        }
        tempErrors.wrongHour =
            (startTime == "00:00" || endTime == "00:00")
                ? "Dates cannot start at 0:00."
                : "";
        setErrors(tempErrors);
        return Object.values(tempErrors).every(err => err === "");
    };
}

export default editWorkHourValidator;