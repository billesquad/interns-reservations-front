export const providerCalendarStyleMainBox = {
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 0,
    alignItems: 'center',
    justifyContent: 'space-between'
}
export const providerCalendarStyleMainList = {width: '100%', p: "3rem", bgcolor: 'background.paper'}
export const providerCalendarStyleDescription = { color: '#737373', fontSize: '16px', marginBottom: '2rem' }