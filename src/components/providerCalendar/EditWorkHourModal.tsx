import {Alert, Box, Button, Dialog, DialogActions, DialogContent, DialogTitle, TextField} from "@mui/material";
import React, {useRef} from "react";
import ObjectData from "@/universalComponents/ObjectDataType";
import {Errors} from "@/components/providerOfferings/providerOfferingsHelpers";
import editWorkHourValidator from "@/components/providerCalendar/utils/editWorkHourValidator";

interface EditWorkHourModalProps {
    open: boolean;
    onClose: () => void;
    workHourId: string;
    onSave: (editedWorkHour: ObjectData) => void;
    onRemove: () => void;
    errors: Errors;
    setErrors: React.Dispatch<React.SetStateAction<Errors>>;
    startTime: string
    setStartTime: React.Dispatch<React.SetStateAction<string>>;
    endTime: string
    setEndTime: React.Dispatch<React.SetStateAction<string>>;
}

const EditWorkHourModal = ({
                               open,
                               onClose,
                               workHourId,
                               onSave,
                               onRemove,
                               errors,
                               setErrors,
                               startTime,
                               setStartTime,
                               endTime,
                               setEndTime,
                           }: EditWorkHourModalProps) => {
    const validate = editWorkHourValidator(startTime, endTime, setErrors);
    const initialStartTime = useRef(startTime);
    const handleSave = () => {
        if (validate()) {
            onSave({workHourId, startTime, endTime});
            onClose();
        } else {
            return;
        }
    };
    const handleRemove = () => {
        onRemove();
        onClose();
    };

    return (
        <Dialog open={open} onClose={onClose} sx={{p: 3}}>
            <DialogTitle>Edit Work Hour</DialogTitle>
            <DialogContent>
                <Box sx={{display: "flex", p: 2}}>
                    <TextField
                        label="Start Time"
                        type="time"
                        value={startTime ? startTime : ''}
                        onChange={(e) => setStartTime(e.target.value)}
                        InputLabelProps={{shrink: true}}
                        inputProps={{step: 60}}
                        sx={{m: 3}}
                        error={!!(errors?.wrongHoursOrder || errors?.wrongHour)}
                    />
                    <TextField
                        label="End Time"
                        type="time"
                        value={endTime ? endTime : ''}
                        onChange={(e) => setEndTime(e.target.value)}
                        InputLabelProps={{shrink: true}}
                        inputProps={{step: 60}}
                        sx={{m: 3}}
                        error={!!(errors?.wrongHoursOrder || errors?.wrongHour)}
                    />

                </Box>
                <Box>
                    {errors?.wrongHour &&
                        <Alert sx={{mb: 1}} severity="error">{errors.wrongHour}</Alert>
                    }
                    {errors?.atLeastOneHour &&
                        <Alert severity="error">{errors.atLeastOneHour}</Alert>
                    }
                    {errors?.emptyFields &&
                        <Alert severity="error">{errors.emptyFields}</Alert>
                    }
                </Box>
            </DialogContent>
            <DialogActions>
                {initialStartTime.current &&
                    <Button onClick={handleRemove} sx={{marginRight: 3}} variant="text">Remove Work Hour</Button>}
                <Button variant="outlined" onClick={onClose}>Cancel</Button>
                <Button variant="outlined" onClick={handleSave}>Save</Button>
            </DialogActions>
        </Dialog>
    );
};

export default EditWorkHourModal;
