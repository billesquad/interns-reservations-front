import {Box, Divider, Grid} from "@mui/material";
import BrowseSideMenu from "@/universalComponents/universalSideMenu/BrowseSideMenu";
import ManyItemDisplay from "@/universalComponents/ManyItemDisplay";
import {useContext, useState} from "react";
import useFetchByUrlParams from "@/universalComponents/universalSideMenu/useFetchByUrlParams";
import useUserContext from "@/components/auth/useUserContext";
import ObjectData from "@/universalComponents/ObjectDataType";
import {UrlParams} from "@/fetching/SendApiRequest";
import {
    INITIAL_URL_PARAMETERS,
    RESERVATION_SORT_PARAMS,
    RESERVATION_STATUSES
} from "@/components/customerReservations/consts/customerReservationsValues";
import {GET_RESERVATIONS_BY_PROVIDER_ID} from "@/fetching/urls/external/providerApiUrls";
import MenuSetStatusFilter from "@/components/customerReservations/MenuSetStatusFilter";
import {useNavigate} from "react-router-dom";
import {providerReservationStyleMainBox} from "@/components/providerReservations/providerReservationsStyle";
import {getProviderReservationsMetadata} from "@/components/providerReservations/getProviderReservationsMetadata";

const ProviderReservations = () => {
    const {userId} = useContext(useUserContext);

    const [forceUpdate, ] = useState(true);
    const [fetchedData, setFetchedData] = useState<ObjectData[]>();
    const [totalElements, setTotalElements] = useState<number>(10);
    const [componentUrlParameters, setComponentUrlParameters] = useState<UrlParams>(INITIAL_URL_PARAMETERS);

    const navigate = useNavigate();


    useFetchByUrlParams(componentUrlParameters, GET_RESERVATIONS_BY_PROVIDER_ID((userId as number).toString()), setFetchedData, forceUpdate, setTotalElements);
    const manyDisplayMetadata = getProviderReservationsMetadata(navigate);

    return (
        <Box sx={providerReservationStyleMainBox}>
            {userId && (
                <Box sx={{width: "100%"}}>
                    <Grid
                        container
                        direction="row"
                        justifyContent="flex-end"
                        alignItems="center"
                        spacing={3}
                        sx={{mb: "2rem"}}>
                        <MenuSetStatusFilter
                            componentUrlParameters={componentUrlParameters}
                            setComponentUrlParameters={setComponentUrlParameters}
                            multiselectParameters={RESERVATION_STATUSES}
                        />
                        <BrowseSideMenu componentUrlParameters={componentUrlParameters}
                                        setComponentUrlParameters={setComponentUrlParameters}
                                        sortParameters={RESERVATION_SORT_PARAMS}
                                        totalElements={totalElements}/>
                    </Grid>
                    <Divider/>
                </Box>
            )}
            {fetchedData && <ManyItemDisplay data={fetchedData} metadata={manyDisplayMetadata}/>}
        </Box>
    )
}

export default ProviderReservations;