import {
    RESERVATION_NAMES,
    RESERVATION_STATUS_DESCRIPTION,
    RESERVATION_STATUS_ICON,
    RESERVATION_STATUSES
} from "@/components/customerReservations/consts/customerReservationsValues";
import {dateValueFormatters} from "@/components/utils/dateValueFormatters";
import ObjectData from "@/universalComponents/ObjectDataType";
import {MOVE_TO_CUSTOMER_OVERVIEW, MOVE_TO_RESERVATION_OVERVIEW} from "@/fetching/urls/internal/internalUrls";
import {NavigateFunction} from "react-router-dom";

export function getProviderReservationsMetadata(navigate: NavigateFunction) {
    return {
        attributeNames: RESERVATION_NAMES,
        excludeKeys: ["reservationId", "offeringId", "providerId", "offeringName", "description", "providerFullName"],
        statusKeyToValue: (value: string) => RESERVATION_STATUSES[value as keyof typeof RESERVATION_STATUSES],
        statusKeyToDescription: (value: string) => RESERVATION_STATUS_DESCRIPTION[value as keyof typeof RESERVATION_STATUS_DESCRIPTION],
        statusKeyToImage: (value: string) => RESERVATION_STATUS_ICON[value as keyof typeof RESERVATION_STATUS_ICON],
        statusKey: "status",
        valueFormatters: {
            reservationDate: dateValueFormatters,
            reservationDateEnd: dateValueFormatters,
            status: (value: string) => RESERVATION_STATUSES[value as keyof typeof RESERVATION_STATUSES],
        },
        menuItems: {
            'See Reservation': (value: ObjectData) => navigate(MOVE_TO_RESERVATION_OVERVIEW(value.reservationId)),
            'See Customer': (value: ObjectData) => navigate(MOVE_TO_CUSTOMER_OVERVIEW(value.customerId)),
        },
        descriptionKey: "description",
        itemName: "offeringName",
        onClickParameter: 'offeringId',
    };
}