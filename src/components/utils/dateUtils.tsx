import WorkHour from "@/entities/WorkHour";

export const formatDuration = (minutes: number): string => {
    const hours = Math.floor(minutes / 60);
    const remainingMinutes = minutes % 60;

    let result = '';

    if (hours > 0) {
        result += `${hours} h`;
    }

    if (remainingMinutes > 0) {
        if (hours > 0) {
            result += ' ';
        }
        result += `${remainingMinutes} min`;
    }

    return result || '0 min';
}

export const isWorkDay = (workHour: WorkHour): boolean => {
    return (!!workHour.startTime && !!workHour.endTime);
}

export const formatWorkHours = (workHour: WorkHour): string => {
    if (!isWorkDay(workHour)) {
        return 'Closed';
    }

    const formatTime = (time: string): string => {
        const noSeconds = time.slice(0, -3);
        return noSeconds.startsWith('0') ? noSeconds.slice(1) : noSeconds;
    }

    const start = formatTime(workHour.startTime.toString());
    const end = formatTime(workHour.endTime.toString());

    return `${start} - ${end}`;
}

export const dayOfWeekMapping: { [key: string]: number } = {
    "Sunday": 0,
    "Monday": 1,
    "Tuesday": 2,
    "Wednesday": 3,
    "Thursday": 4,
    "Friday": 5,
    "Saturday": 6,
};

export const formatDateToCustomFormat = (dateString: string) => {
    const date = new Date(dateString);

    const day = String(date.getDate()).padStart(2, '0');
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const year = date.getFullYear();

    const hours = String(date.getHours()).padStart(2, '0');
    const minutes = String(date.getMinutes()).padStart(2, '0');

    return `${day}/${month}/${year} at ${hours}:${minutes}`;
}

export const getTime = (date: Date): string => {
    const hours = date.getHours();
    const minutes = String(date.getMinutes()).padStart(2, '0');
    return `${hours}:${minutes}`;
};

export const isLessThanOneDayBeforeStart = (startDateStr: string): boolean => {
    const now = new Date();
    const startDate = new Date(startDateStr);
    const timeDifference = startDate.getTime() - now.getTime();
    const dayInMilliseconds = 24 * 60 * 60 * 1000;

    return timeDifference < dayInMilliseconds;
}
