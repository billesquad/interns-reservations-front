function convertTimeToMinutes(timeStr: string) {
    const match = timeStr.match(/(\d{2}):(\d{2})/);

    if (!match) {
        throw new Error("Invalid time format");
    }

    const hours = parseInt(match[1], 10);
    const minutes = parseInt(match[2], 10);

    return hours * 60 + minutes;
}

export default convertTimeToMinutes;