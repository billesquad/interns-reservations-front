export const formatReservationStatus = (status: string | null): string => {
    const formatFirstLetterToUpper = (str: string) => {
        return str.charAt(0).toUpperCase() + str.slice(1);
    };

    return status ? formatFirstLetterToUpper(status.toLowerCase().replace(/_/g, ' ')) : "";
}

export const canBeCancelled = (status: string): boolean => {
    return !(status === "CANCELLED" || status ===  "DECLINED" || status ===  "DONE");
}

export const isCancelled = (status: string | undefined): boolean => {
    if (status) {
        return status === "CANCELLED";
    } 
    return false;
}