import { Box, Card, CardContent, CircularProgress, Grid, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { useLocation, useParams } from "react-router-dom";
import User from "@/entities/User";
import { fetchProviderById, fetchAddressById, fetchCustomerById } from "@/components/userOverwiev/UserOverviewService";
import Address from "@/entities/Address";
import UserDetails from "@/components/userOverwiev/UserDetails";
import AddressDetails from "@/components/userOverwiev/AddressDetails";
import { boxProgressStyle, cardContentStyle, cardStyle, titleStyle } from "@/components/userOverwiev/UserOverwievStyles";

const UserOverview = () => {
    const { id } = useParams();
    const [user, setUser] = useState<User | null>(null);
    const [addressId, setAddressId] = useState<string | null>(null);
    const [address, setAddress] = useState<Address | null>(null);

    const userType = useUserType();
    const detailsText = useDetailsText(userType);

    useEffect(() => {
        const fetchData = async () => {
            try {
                let data;
                if (userType === 'provider') {
                    data = await fetchProviderById(id!);
                } else if (userType === 'customer') {
                    data = await fetchCustomerById(id!);
                }
                setUser(data);
                setAddressId(data.addressId);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };
        fetchData();
    }, [id, userType]);

    useEffect(() => {
        const fetchData = async () => {
            if (addressId) {
                try {
                    const data = await fetchAddressById(addressId!);
                    setAddress(data);
                } catch (error) {
                    console.error('Error fetching address:', error);
                }
            }
        };
        fetchData();
    }, [addressId]);

    if (!user) {
        return (
            <Box style={boxProgressStyle}>
                <CircularProgress />
            </Box>
        );
    }

    return (
        (<Box>
            <Typography style={titleStyle}>
                {detailsText}
            </Typography>
            <Card elevation={3} style={cardStyle}>
                <CardContent style={cardContentStyle}>
                    <Grid container>
                        <UserDetails user={user} userType={userType} userId={id} />
                        <AddressDetails address={address} />
                    </Grid>
                </CardContent>
            </Card>
        </Box>)
    );
};

export default UserOverview;

const useUserType = () => {
    const location = useLocation();
    let userType = '';

    if (location.pathname.includes('/provider/')) {
        userType = 'provider';
    } else if (location.pathname.includes('/customer/')) {
        userType = 'customer';
    }

    return userType;
}

const useDetailsText = (userType: string) => {
    let detailsText = '';

    if (userType === 'provider') {
        detailsText = "Provider's Details";
    } else if (userType === 'customer') {
        detailsText = "Customer's Details";
    }

    return detailsText;
}
