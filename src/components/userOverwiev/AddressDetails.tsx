import { Grid, Typography } from "@mui/material";
import FlagIcon from '@mui/icons-material/Flag';
import LocationCityIcon from '@mui/icons-material/LocationCity';
import SignpostIcon from '@mui/icons-material/Signpost';
import MarkunreadMailboxIcon from '@mui/icons-material/MarkunreadMailbox';
import FormatListNumberedIcon from '@mui/icons-material/FormatListNumbered';
import Address from "@/entities/Address";
import { addressStyle, iconStyle, notProvidedStyle, marginTopTextStyle, titleStyle, valueStyle } from "@/components/userOverwiev/UserOverwievStyles";

interface AddressDetailsProps {
    address: Address | null;
}

const AddressDetails: React.FC<AddressDetailsProps> = ({ address }) => {
    const addressFields = addressFieldsAreNull(address);

    return (
        <Grid item xs={5}>
            <Typography style={titleStyle}>
                Address
            </Typography>
            {addressFields ? (
                <Typography style={addressStyle}>
                    Address not provided
                </Typography>
            ) : (
                <>
                    <Typography style={marginTopTextStyle} >
                        <FlagIcon style={iconStyle} />
                        Country:
                    </Typography>
                    {renderString(address?.country)}

                    <Typography style={marginTopTextStyle} >
                        <LocationCityIcon style={iconStyle} />
                        City:
                    </Typography>
                    {renderString(address?.city)}

                    <Typography style={marginTopTextStyle} >
                        <SignpostIcon style={iconStyle} />
                        Street:
                    </Typography>
                    {renderString(address?.street)}

                    <Typography style={marginTopTextStyle} >
                        <MarkunreadMailboxIcon style={iconStyle} />
                        Zip Code:
                    </Typography>
                    {renderString(address?.zipCode)}

                    <Typography style={marginTopTextStyle} >
                        <FormatListNumberedIcon style={iconStyle} />
                        Building Number:
                    </Typography>
                    {renderString(address?.buildingNumber)}

                    <Typography style={marginTopTextStyle} >
                        <FormatListNumberedIcon style={iconStyle} />
                        Apartment Number:
                    </Typography>
                    {renderString(address?.apartmentNumber)}
                </>
            )}
        </Grid>
    );
};

export default AddressDetails;

const renderString = (value: string | null | undefined) => (
    <Typography style={value ? valueStyle : notProvidedStyle}>
        {value || 'Not provided'}
    </Typography>
);

const addressFieldsAreNull = (address: Address | null): boolean => 
    !address || (!address.country &&
        !address.city &&
        !address.street &&
        !address.zipCode &&
        !address.buildingNumber &&
        !address.apartmentNumber);
