import { Grid, Typography } from "@mui/material";
import PersonIcon from '@mui/icons-material/Person';
import EmailIcon from '@mui/icons-material/Email';
import LocalPhoneIcon from '@mui/icons-material/LocalPhone';
import User from "@/entities/User";
import { textStyle, valueStyle, specializationStyle, marginTopTextStyle, titleStyle } from "@/components/userOverwiev/UserOverwievStyles";
import ProviderWorkHours from "@/components/userOverwiev/ProviderWorkHours";

interface UserDetailsProps {
    user: User;
    userType: string;
    userId: string | undefined;
}

const UserDetails: React.FC<UserDetailsProps> = ({ user, userType, userId }) => {
    return (
        <Grid item xs={7} >
            <Typography style={titleStyle}>
                Personal Data
            </Typography>

            <Typography style={marginTopTextStyle} >
                <PersonIcon style={textStyle} />
                Name:
            </Typography>
            <Typography style={valueStyle}>
                {user.firstName} {user.lastName}
            </Typography>
            <Typography style={specializationStyle}>
                {user?.specialization}
            </Typography>

            <Typography style={marginTopTextStyle} >
                <EmailIcon style={textStyle} />
                E-mail:
            </Typography>
            <Typography style={valueStyle}>
                {user.email}
            </Typography>

            <Typography style={marginTopTextStyle} >
                <LocalPhoneIcon style={textStyle} />
                Phone number:
            </Typography>
            <Typography style={valueStyle}>
                {user.phoneNumber}
            </Typography>

            {userType === "provider" && (
                <ProviderWorkHours providerId={userId} />
            )}
        </Grid>
    );
};

export default UserDetails;
