import RequestTypes from "@/enums/RequestTypes";
import SendApiRequest from "@/fetching/SendApiRequest";
import { GET_ADDRESS_BY_ID, GET_CUSTOMER_BY_ID, GET_PROVIDER_BY_ID, GET_PROVIDER_CALENDAR } from "@/fetching/urls/external/commonApiUrls";

export const fetchProviderById = async (providerId: string) => {
    return await SendApiRequest({
        partOfUrl: GET_PROVIDER_BY_ID(providerId),
        typeOfRequest: RequestTypes.GET
    });
}

export const fetchCustomerById = async (customerId: string) => {
    return await SendApiRequest({
        partOfUrl: GET_CUSTOMER_BY_ID(customerId),
        typeOfRequest: RequestTypes.GET
    });
}

export const fetchAddressById = async (addressId: string) => {
    return await SendApiRequest({
        partOfUrl: GET_ADDRESS_BY_ID(addressId),
        typeOfRequest: RequestTypes.GET
    });
}

export const fetchProviderCalendarById = async (providerId: string) => {
    return await SendApiRequest({
        partOfUrl: GET_PROVIDER_CALENDAR(providerId),
        typeOfRequest: RequestTypes.GET
    });
}
