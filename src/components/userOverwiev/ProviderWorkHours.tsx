import { Box, Grid, Typography } from "@mui/material";
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import { formatWorkHours } from "@/components/utils/dateUtils";
import { boxStyle, marginTopTextStyle, nameStyle, spaceBetweenStyle, textStyle } from "@/components/userOverwiev/UserOverwievStyles";
import { useEffect, useState } from "react";
import WorkHour from "@/entities/WorkHour";
import { fetchProviderCalendarById } from "@/components/userOverwiev/UserOverviewService";

interface ProviderWorkHoursProps {
    providerId: string | undefined;
}

const ProviderWorkHours: React.FC<ProviderWorkHoursProps> = ({ providerId }) => {
    const [calendar, setCalendar] = useState<WorkHour[] | null>(null);

    useEffect(() => {
        const fetchData = async () => {
            if (providerId) {
                try {
                    const data = await fetchProviderCalendarById(providerId);
                    setCalendar(data);
                } catch (error) {
                    console.error('Error fetching calendar:', error);
                }
            }
        };
        fetchData();
    }, [providerId]);

    return (
        calendar && 
        <Box style={boxStyle}>
            <Typography style={marginTopTextStyle}>
                <CalendarMonthIcon style={textStyle} />
                Work Calendar
            </Typography>

            <Grid container style={spaceBetweenStyle}>
                {calendar.map(hours => (
                    <Grid item key={hours.workHourId}>
                        <Typography style={nameStyle} key={hours.workHourId}>
                            {hours.dayOfWeek}<br /> {formatWorkHours(hours)}
                        </Typography>
                    </Grid>
                ))}
            </Grid>
        </Box>
    )
}

export default ProviderWorkHours;
