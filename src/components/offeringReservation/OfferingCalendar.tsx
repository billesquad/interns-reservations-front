import { useState, useEffect } from 'react';
import { Box, Button, Typography } from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import dayjs from 'dayjs';
import { DateCalendar } from '@mui/x-date-pickers/DateCalendar';
import WorkHour from '@/entities/WorkHour';
import TimeSelectionModal from '@/components/offeringReservation/TimeSelectionModal';
import TimeRange from '@/entities/TimeRange';
import CircularProgress from '@mui/material/CircularProgress';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import { fetchReservationsByProviderAndDay } from '@/components/offeringReservation/OfferingReservationService';
import { boxStyle, buttonStyle, marginRightStyle, titleStyle } from '@/components/offeringReservation/OfferingReservationStyles';
import { getDisabledDays, transformToClockFormat, getWorkHoursForSelectedDay } from '@/components/offeringReservation/timeUtils';

interface OfferingCalendarProps {
    calendar: WorkHour[] | undefined;
    providerId: number | undefined;
    offeringDuration: number | undefined;
    onSelectedDateReserve: (time: dayjs.Dayjs | null) => void;
}

const OfferingCalendar: React.FC<OfferingCalendarProps> = ({ calendar, providerId, offeringDuration, onSelectedDateReserve }) => {
    const [selectedDate, setSelectedDate] = useState<dayjs.Dayjs | null>(null);
    const [disabledTimes, setDisabledTimes] = useState<TimeRange[]>([]);
    const [isLoading, setIsLoading] = useState(false);
    const [openModal, setOpenModal] = useState(false);

    const disabledDays = getDisabledDays(calendar);

    useEffect(() => {
        const fetchData = async () => {
            if (!selectedDate || !providerId) return;

            setIsLoading(true);
            try {
                const reservationTimesData = await fetchReservationsByProviderAndDay(providerId.toString(), selectedDate);
                if (reservationTimesData) {
                    setDisabledTimes(reservationTimesData.map(transformToClockFormat(selectedDate)));
                }
            } catch (error) {
                console.error('Error fetching data:', error);
            } finally {
                setIsLoading(false);
            }
        };
        fetchData();
    }, [selectedDate, providerId]);

    return (
        <Box sx={boxStyle} >
            <Typography style={titleStyle}>
                Select reservation date
            </Typography>

            <LocalizationProvider dateAdapter={AdapterDayjs}>
                <DateCalendar
                    value={selectedDate}
                    views={['year', 'month', 'day']}
                    showDaysOutsideCurrentMonth fixedWeekNumber={6}
                    onChange={setSelectedDate}
                    dayOfWeekFormatter={(day) => `${day}.`}
                    disablePast
                    shouldDisableDate={date => disabledDays.includes(date.day())}
                />
            </LocalizationProvider>

            <Button
                variant="contained"
                disabled={!selectedDate || isLoading}
                onClick={() => setOpenModal(true)}
                sx={buttonStyle}
            >
                {isLoading ? (
                    <CircularProgress size={24} color="inherit" />
                ) : (
                    <>
                        <AccessTimeIcon style={marginRightStyle} />
                        Select an Hour
                    </>
                )}
            </Button>

            {!isLoading && <TimeSelectionModal
                open={openModal}
                onClose={() => { setOpenModal(false); }}
                workHours={getWorkHoursForSelectedDay(selectedDate, calendar)}
                disabledTimes={disabledTimes}
                selectedDate={selectedDate}
                offeringDuration={offeringDuration}
                onSelectedDateReserve={onSelectedDateReserve}
            />}
        </Box>
    );
}

export default OfferingCalendar;

