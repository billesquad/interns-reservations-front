import { useState } from 'react';
import { Box, Button, Grid, Typography } from '@mui/material';
import dayjs from 'dayjs';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { TimeClock } from '@mui/x-date-pickers/TimeClock';
import TimeRange from '@/entities/TimeRange';
import DoneIcon from '@mui/icons-material/Done';
import CloseIcon from '@mui/icons-material/Close';
import { modalButtonStyle, buttonsContainerStyle, marginRightStyle, nameStyle, modalGridStyle } from '@/components/offeringReservation/OfferingReservationStyles';
import { isHourDisabled, isMinuteDisabled } from '@/components/offeringReservation/timeUtils';

interface OfferingTimePickerProps {
    disabledTimes: TimeRange[];
    minTime: dayjs.Dayjs;
    maxTime: dayjs.Dayjs;
    selectedDate: dayjs.Dayjs | null;
    onClose: () => void;
    offeringDuration: number | undefined;
    onSelectedDateReserve: (time: dayjs.Dayjs | null) => void;
}

const OfferingTimePicker: React.FC<OfferingTimePickerProps> = (
    { disabledTimes, minTime, maxTime, onClose, selectedDate, offeringDuration, onSelectedDateReserve }
) => {
    const [selectedTime, setSelectedTime] = useState<dayjs.Dayjs | null>(null);
    const [view, setView] = useState<'hours' | 'minutes'>('hours');
    const [hasSelectedMinutes, setHasSelectedMinutes] = useState(false);

    const isTimeDisabled = (time: dayjs.Dayjs, viewType: 'hours' | 'minutes' | 'seconds'): boolean => {
        const date = selectedDate;
        if (!date || !offeringDuration) return false;

        if (viewType === 'hours') return isHourDisabled(time, date, disabledTimes, offeringDuration, maxTime);
        if (viewType === 'minutes') return isMinuteDisabled(time, date, disabledTimes, offeringDuration, maxTime);

        return false;
    };

    const handleTimeChange = (timeValue: dayjs.Dayjs | null) => {
        if (timeValue) {
            setSelectedTime(timeValue);
            if (view === 'hours') {
                setView('minutes');
                setHasSelectedMinutes(false);
            } else if (view === 'minutes') {
                setHasSelectedMinutes(true);
            }
        }
    };

    const handleCancelClick = () => {
        if (view === 'minutes') {
            setView('hours');
        } else {
            onClose();
        }
        setSelectedTime(null);
        setHasSelectedMinutes(false);
    };

    const handleReserveClick = () => {
        if (selectedDate && selectedTime) {
            const fullDate = selectedDate.hour(selectedTime.hour()).minute(selectedTime.minute());
            onSelectedDateReserve(fullDate);
        }
        onClose();
    };

    return (
        <Box>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
                <Grid container style={modalGridStyle}>
                    <Grid item xs={6}>
                        <TimeClock
                            views={[view]}
                            ampm={false}
                            value={selectedTime}
                            minTime={minTime}
                            maxTime={maxTime}
                            onChange={handleTimeChange}
                            shouldDisableTime={(timeValue, viewType) => {
                                if (viewType === 'seconds') return false;
                                return isTimeDisabled(timeValue, viewType);
                            }}
                        />
                    </Grid>

                    <Grid item xs={5.5}>
                        <Typography variant="h6">
                            Selected date: {selectedDate?.format('DD.MM.YYYY')}
                        </Typography>

                        <Typography variant="h6">
                            Selected time: {renderSelectedTimeText(selectedTime, hasSelectedMinutes)}
                        </Typography>

                        <Grid container style={buttonsContainerStyle}>
                            <Grid item xs={2} ml={2.5}>
                                <Button variant="outlined" onClick={handleCancelClick} style={modalButtonStyle}>
                                    <CloseIcon style={marginRightStyle} />
                                    Cancel
                                </Button>
                            </Grid>
                            <Grid item xs={6}>
                                <Button variant="contained" disabled={!selectedTime || !hasSelectedMinutes} onClick={handleReserveClick} style={modalButtonStyle}>
                                    <DoneIcon style={marginRightStyle} />
                                    Reserve
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </LocalizationProvider>
        </Box>
    );
}

export default OfferingTimePicker;

const renderSelectedTimeText = (selectedTime: dayjs.Dayjs | null, hasSelectedMinutes: boolean) => {
    if (selectedTime) {
        return hasSelectedMinutes
            ? <span style={{ fontSize: 'inherit' }}>{selectedTime.format('HH:mm')}</span>
            : <span style={nameStyle}>minute not selected</span>;
    } else {
        return <span style={nameStyle}>hour not selected</span>;
    }
};