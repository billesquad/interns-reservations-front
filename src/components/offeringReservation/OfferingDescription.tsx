import { Box, Grid, Typography } from '@mui/material';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import SellIcon from '@mui/icons-material/Sell';
import PersonIcon from '@mui/icons-material/Person';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import Offering from '@/entities/Offering';
import { formatDuration, formatWorkHours } from '@/components/utils/dateUtils';
import WorkHour from '@/entities/WorkHour';
import { offeringNameStyle, descriptionStyle, nullDescriptionStyle, secondaryTextStyle, iconStyle, nameStyle, specializationStyle, calendarStyle, linkStyle, spaceBetweenStyle } from '@/components/offeringReservation/OfferingReservationStyles';
import { PROVIDER_OVERVIEW } from '@/fetching/urls/internal/internalUrls';
import { Link } from 'react-router-dom';

interface OfferingDescriptionProps {
    offering: Offering;
    calendar: WorkHour[];
}

const OfferingDescription: React.FC<OfferingDescriptionProps> = ({ offering, calendar }) => {
    const providerUrl = `${PROVIDER_OVERVIEW}/${offering.providerId}`;

    return (
        <Box>
            <Typography style={offeringNameStyle}>
                {offering.offeringName}
            </Typography>

            {offering.description !== "" ? (
                <Typography
                    style={descriptionStyle}>
                    {offering.description}
                </Typography>
            ) : (
                <Typography
                    style={nullDescriptionStyle}>
                    Provider has not added a description
                </Typography>
            )}

            <Grid container style={spaceBetweenStyle}>
                <Grid item>
                    <Typography style={secondaryTextStyle}>
                        <AccessTimeIcon style={iconStyle} />
                        {formatDuration(offering.duration)}
                    </Typography>

                    <Typography style={secondaryTextStyle} >
                        <SellIcon style={iconStyle} />
                        {offering.price.toFixed(2)} {offering.currency}
                    </Typography>
                </Grid>

                <Grid item>
                    <Typography style={nameStyle} >
                        <Link to={providerUrl} style={linkStyle}>
                            <PersonIcon style={iconStyle} />
                            {offering.providerFullName}
                        </Link>
                    </Typography>
                    <Typography style={specializationStyle}>
                        {offering.providerSpecialization}
                    </Typography>
                </Grid>
            </Grid>

            <Typography style={calendarStyle}>
                <CalendarMonthIcon style={iconStyle} />
                Work Calendar
            </Typography>

            <Grid container style={spaceBetweenStyle}>
                {calendar.map(hours => (
                    <Grid item key={hours.workHourId}>
                        <Typography
                            style={nameStyle}
                            key={hours.workHourId}>
                            {hours.dayOfWeek}<br /> {formatWorkHours(hours)}
                        </Typography>
                    </Grid>
                ))}
            </Grid>

        </Box>
    );
}

export default OfferingDescription;
