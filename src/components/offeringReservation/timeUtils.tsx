import { dayOfWeekMapping, isWorkDay } from "@/components/utils/dateUtils";
import ReservationTime from "@/entities/ReservationTime";
import TimeRange from "@/entities/TimeRange";
import WorkHour from "@/entities/WorkHour";
import dayjs from "dayjs";

export const isHourDisabled = (time: dayjs.Dayjs, date: dayjs.Dayjs, disabledTimes: TimeRange[], offeringDuration: number, maxTime: dayjs.Dayjs): boolean => {
    const startTimeCheck = date.hour(time.hour()).startOf('hour');

    for (let i = 0; i < 60; i++) {
        const checkTime = startTimeCheck.add(i, 'minute');
        const endTimeCheck = checkTime.add(offeringDuration, 'minutes');
        if (endTimeCheck.isAfter(maxTime)) {
            continue;
        }
        if (!isTimeInRange(checkTime, disabledTimes, offeringDuration)) {
            if (endTimeCheck.hour() !== checkTime.hour() && isTimeInRange(endTimeCheck.subtract(offeringDuration, 'minutes'), disabledTimes, offeringDuration)) {
                continue;
            }
            return false;
        }
    }
    return true;
};

export const isMinuteDisabled = (time: dayjs.Dayjs, date: dayjs.Dayjs, disabledTimes: TimeRange[], offeringDuration: number, maxTime: dayjs.Dayjs): boolean => {
    const checkTime = date.hour(time.hour()).minute(time.minute());
    const endTimeCheck = checkTime.add(offeringDuration, 'minutes');

    if (endTimeCheck.isAfter(maxTime)) {
        return true;
    }

    return isTimeInRange(checkTime, disabledTimes, offeringDuration);
};

export const isTimeInRange = (time: dayjs.Dayjs, disabledTimes: TimeRange[], offeringDuration: number): boolean => {
    return disabledTimes.some(range => {
        const startTimeAdjusted = range.startTime.subtract(offeringDuration - 1, 'minutes');
        return (
            (time.isBefore(range.endTime) || time.isSame(range.endTime)) &&
            (time.isAfter(startTimeAdjusted) || time.isSame(startTimeAdjusted))
        );
    });
};


export const getDisabledDays = (calendar: WorkHour[] | undefined): number[] => {
    if (!calendar) return [];
    return calendar.filter(day => !isWorkDay(day)).map(wh => dayOfWeekMapping[wh.dayOfWeek]);
}

export const getWorkHoursForSelectedDay = (date: dayjs.Dayjs | null, calendar: WorkHour[] | undefined): WorkHour | undefined => {
    if (!date || !calendar) return undefined;
    const selectedDayOfWeek = date.format('dddd').toLowerCase();
    return calendar.find(workHour => workHour.dayOfWeek.toLowerCase() === selectedDayOfWeek);
}

export const transformToClockFormat = (date: dayjs.Dayjs) => (reservationTime: ReservationTime) => ({
    startTime: dayjs(`${date.format('YYYY-MM-DD')} ${reservationTime.reservationStartTime.toString().substring(0, 5)}`, 'YYYY-MM-DD HH:mm'),
    endTime: dayjs(`${date.format('YYYY-MM-DD')} ${reservationTime.reservationEndTime.toString().substring(0, 5)}`, 'YYYY-MM-DD HH:mm').subtract(1, 'minute')
});

export const getMinMaxTimeValues = (
    workHours: WorkHour | undefined,
    selectedDate: dayjs.Dayjs | null): [dayjs.Dayjs, dayjs.Dayjs] => {
    let minTimeValue = dayjs().startOf('day');
    let maxTimeValue = dayjs().endOf('day');

    if (!workHours || !selectedDate) return [minTimeValue, maxTimeValue];

    minTimeValue = dayjs(`${selectedDate.format('YYYY-MM-DD')}T${workHours.startTime}`);
    maxTimeValue = dayjs(`${selectedDate.format('YYYY-MM-DD')}T${workHours.endTime}`);

    return [minTimeValue, maxTimeValue];
}
