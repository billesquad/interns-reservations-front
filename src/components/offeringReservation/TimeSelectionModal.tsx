import { Box, Modal, Backdrop, Fade, Typography } from '@mui/material';
import dayjs from 'dayjs';
import OfferingTimePicker from '@/components/offeringReservation/OfferingTimePicker';
import WorkHour from '@/entities/WorkHour';
import { modalStyle, modalTitleStyle } from '@/components/offeringReservation/OfferingReservationStyles';
import { getMinMaxTimeValues } from '@/components/offeringReservation/timeUtils';

interface TimeSelectionModalProps {
    open: boolean;
    onClose: () => void;
    workHours: WorkHour | undefined;
    disabledTimes: ClockTime[];
    selectedDate: dayjs.Dayjs | null;
    offeringDuration: number | undefined;
    onSelectedDateReserve: (time: dayjs.Dayjs | null) => void;
}

type ClockTime = {
    startTime: dayjs.Dayjs;
    endTime: dayjs.Dayjs;
};

const TimeSelectionModal: React.FC<TimeSelectionModalProps> = (
    { open, onClose, workHours, disabledTimes, selectedDate, offeringDuration, onSelectedDateReserve }
) => {
    const [minTime, maxTime] = getMinMaxTimeValues(workHours, selectedDate);

    return (
        <Modal
            open={open}
            onClose={onClose}
            closeAfterTransition
            slots={{ backdrop: Backdrop }}
            slotProps={{ backdrop: { timeout: 500 } }}
        >
            <Fade in={open}>
                <Box sx={modalStyle}>
                    <Typography style={modalTitleStyle}>
                        Select reservation time
                    </Typography>
                    <OfferingTimePicker
                        disabledTimes={disabledTimes}
                        minTime={minTime}
                        maxTime={maxTime}
                        onClose={onClose}
                        selectedDate={selectedDate}
                        offeringDuration={offeringDuration}
                        onSelectedDateReserve={onSelectedDateReserve}
                    />
                </Box>
            </Fade>
        </Modal>
    );
};

export default TimeSelectionModal;
