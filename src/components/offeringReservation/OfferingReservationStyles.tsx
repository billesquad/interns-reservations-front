export const boxStyle = {display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', height: '100%', };
export const buttonStyle = { marginTop: '80px', width: '200px', height: '40px' };
export const offeringNameStyle = { marginBottom: '16px', fontSize: '28px', fontWeight: 'bold' };
export const descriptionStyle: React.CSSProperties = { marginBottom: '20px', fontSize: '16px', textAlign: 'justify' };
export const nullDescriptionStyle = { marginBottom: '20px', fontSize: '12px', color: '#737373' };
export const secondaryTextStyle: React.CSSProperties = { fontSize: '16px', color: '#737373', textAlign: 'left' };
export const iconStyle = { fontSize: '16px', color: '#737373', marginRight: '4px' };
export const nameStyle = { fontSize: '16px', color: '#737373' };
export const specializationStyle = { fontSize: '12px', color: '#858585', marginLeft: '20px' };
export const calendarStyle: React.CSSProperties = { fontSize: '16px', marginBottom: '4px', marginTop: '16px', color: '#737373', textAlign: 'center'};
export const linkStyle = { textDecoration: 'none', color: 'inherit' };
export const modalTitleStyle: React.CSSProperties = { textAlign: 'center', marginBottom: '20px', fontSize: '24px' };
export const modalButtonStyle = { width: '120px', height: '40px' };
export const buttonsContainerStyle = { marginTop: '50px', justifyContent: 'space-between' };
export const marginRightStyle = { marginRight: '4px' };
export const titleStyle = { marginBottom: '32px', fontSize: '28px' };
export const spaceBetweenStyle = { justifyContent: 'space-between' };
export const modalGridStyle = { alignItems: 'center', justifyContent: 'space-between' };

export const modalStyle = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '40%',
    bgcolor: 'background.paper',
    border: '1px solid',
    borderRadius: '16px',
    boxShadow: 24,
    p: 4,
    height: '370px'
};
