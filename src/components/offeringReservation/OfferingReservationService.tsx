import SendApiRequest from '@/fetching/SendApiRequest';
import RequestTypes from '@/enums/RequestTypes';
import { GET_OFFERING_BY_ID, GET_PROVIDER_CALENDAR, GET_RESERVATIONS_BY_PROVIDER_AND_DAY } from '@/fetching/urls/external/commonApiUrls';
import { CREATE_RESERVATION } from '@/fetching/urls/external/customerApiUrls';
import Reservation from '@/entities/Reservation';
import dayjs from 'dayjs';

export const fetchOfferingById = async (offeringId: string) => {
    return await SendApiRequest({
        partOfUrl: GET_OFFERING_BY_ID(offeringId),
        typeOfRequest: RequestTypes.GET
    });
}

export const fetchProviderCalendar = async (providerId: string) => {
    return await SendApiRequest({
        partOfUrl: GET_PROVIDER_CALENDAR(providerId),
        typeOfRequest: RequestTypes.GET
    });
}

export const fetchReservationsByProviderAndDay = async (providerId: string, selectedDate: dayjs.Dayjs) => {
    return await SendApiRequest({
        partOfUrl: GET_RESERVATIONS_BY_PROVIDER_AND_DAY,
        typeOfRequest: RequestTypes.GET,
        urlParameters: {
            providerId: providerId,
            date: selectedDate.format('YYYY-MM-DD')
        }
    });
}

export const createReservation = async (customerId: number, offeringId: number, date: string): Promise<Reservation> => {
    const response = await SendApiRequest({
        partOfUrl: CREATE_RESERVATION,
        typeOfRequest: RequestTypes.POST,
        body: {
            customerId: customerId,
            offeringId: offeringId,
            reservationStartDate: date
        }
    });

    return {
        reservationId: response.reservationId,
        customerId: response.customerId,
        offeringId: response.offeringId,
        providerId: response.providerId,
        offeringName: response.offeringName,
        price: response.price,
        currency: response.currency,
        description: response.description,
        providerFullName: response.providerFullName,
        providerSpecialization: response.providerSpecialization,
        customerFullName: response.customerFullName,
        reservationDate: response.reservationDate,
        reservationDateEnd: response.reservationDateEnd,
        status: response.status,
    };
    
}
