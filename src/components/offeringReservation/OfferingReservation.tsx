import { useState, useEffect, useContext } from 'react';
import { Box, Grid } from '@mui/material';
import { useParams, useNavigate } from 'react-router-dom';
import OfferingDescription from '@/components/offeringReservation/OfferingDescription';
import OfferingCalendar from '@/components/offeringReservation/OfferingCalendar';
import Offering from '@/entities/Offering';
import WorkHour from '@/entities/WorkHour';
import useUserContext from '@/components/auth/useUserContext';
import dayjs from 'dayjs';
import { useSnackbar } from '@/informationSnackbar/SnackbarProvider';
import { RESERVATION } from '@/fetching/urls/internal/internalUrls';
import { createReservation, fetchOfferingById, fetchProviderCalendar } from '@/components/offeringReservation/OfferingReservationService';

const OfferingReservation = () => {
    const [offering, setOffering] = useState<Offering>();
    const [calendar, setCalendar] = useState<WorkHour[]>();
    const [selectedDateReserve, setSelectedDateReserve] = useState<dayjs.Dayjs | null>(null);
    const navigate = useNavigate();
    const { openSnackbar } = useSnackbar();

    const { id } = useParams<{ id: string }>();
    const { userId } = useContext(useUserContext);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const offeringData = await fetchOfferingById(id!);
                setOffering(offeringData);

                if (offeringData?.providerId) {
                    const calendarData = await fetchProviderCalendar(offeringData.providerId);
                    setCalendar(calendarData);
                }
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };
        fetchData();
    }, [id]);

    const handleReserve = async (dateToReserve: dayjs.Dayjs) => {
        if (!userId || !id) {
            console.error('User Id or Offering Id missing');
            return;
        }
    
        try {
            const mappedResponse = await createReservation(userId!, Number(id), dateToReserve.format('YYYY-MM-DDTHH:mm'));

            navigate(RESERVATION + `/${mappedResponse.reservationId}`);
            openSnackbar('Successfully reserved.', 'success');
        } catch (error) {
            openSnackbar('Failed to make a reservation.', 'error');
        }
    };

    useEffect(() => {
        if (selectedDateReserve) {
            handleReserve(selectedDateReserve);
        }
    }, [selectedDateReserve]);

    return (
        <Box>
            <Grid container spacing={2}>
                <Grid item xs={5.6}>
                    {offering && calendar && <OfferingDescription offering={offering} calendar={calendar} />}
                </Grid>
                <Grid item xs={6.4}>
                    <OfferingCalendar
                        calendar={calendar}
                        providerId={offering?.providerId}
                        offeringDuration={offering?.duration}
                        onSelectedDateReserve={setSelectedDateReserve}
                    />
                </Grid>
            </Grid>
        </Box>
    );
}

export default OfferingReservation;
