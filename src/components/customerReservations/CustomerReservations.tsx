import {Box, Typography} from "@mui/material";
import {useContext, useState} from "react";
import useUserContext from "@/components/auth/useUserContext";
import ObjectData from "@/universalComponents/ObjectDataType";
import ManyItemDisplay from "@/universalComponents/ManyItemDisplay";
import {UrlParams} from "@/fetching/SendApiRequest";
import {useNavigate} from "react-router-dom";
import {INITIAL_URL_PARAMETERS} from "@/components/customerReservations/consts/customerReservationsValues";
import ReservationSideMenu from "@/components/customerReservations/ReservationSideMenu";
import useFetchDataFromUrlParams from "@/universalComponents/universalSideMenu/useFetchByUrlParams";
import {GET_CUSTOMER_RESERVATIONS} from "@/fetching/urls/external/customerApiUrls";
import getCustomerReservationMetadata from "@/components/customerReservations/getCustomerReservationMetadata";
import {customerReservationsMainBoxStyle} from "@/components/customerReservations/consts/customerReservationsStyle";

const CustomerReservations = () => {
    const {userId} = useContext(useUserContext);
    const navigate = useNavigate();

    const [forceUpdate,] = useState(true);
    const [totalElements, setTotalElements] = useState<number>(10);
    const [fetchedData, setFetchedData] = useState<ObjectData[]>();
    const [componentUrlParameters, setComponentUrlParameters] = useState<UrlParams>(INITIAL_URL_PARAMETERS);
    const manyDisplayMetadata = getCustomerReservationMetadata(navigate);

    useFetchDataFromUrlParams(componentUrlParameters, GET_CUSTOMER_RESERVATIONS((userId as number).toString()), setFetchedData, forceUpdate, setTotalElements);

    return (
        <>
            <Typography variant="h4" align="left" color="primary" sx={{mb: "2rem"}} gutterBottom>Browse
                Reservations</Typography>
            <Box sx={customerReservationsMainBoxStyle}>
                {userId && <ReservationSideMenu componentUrlParameters={componentUrlParameters}
                                                setComponentUrlParameters={setComponentUrlParameters}
                                                totalElements={totalElements}/>}
                {fetchedData && <ManyItemDisplay data={fetchedData} metadata={manyDisplayMetadata}/>}
            </Box>
        </>
    )
}

export default CustomerReservations;