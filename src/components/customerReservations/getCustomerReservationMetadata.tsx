import {
    RESERVATION_NAMES,
    RESERVATION_STATUS_DESCRIPTION,
    RESERVATION_STATUS_ICON,
    RESERVATION_STATUSES
} from "@/components/customerReservations/consts/customerReservationsValues";
import ObjectData from "@/universalComponents/ObjectDataType";
import {dateValueFormatters} from "@/components/utils/dateValueFormatters";
import {MOVE_TO_PROVIDER_OVERVIEW, MOVE_TO_RESERVATION_OVERVIEW} from "@/fetching/urls/internal/internalUrls";
import {NavigateFunction} from "react-router-dom";

const getCustomerReservationMetadata = (navigate: NavigateFunction) => {
    return {
        attributeNames: RESERVATION_NAMES,
        itemName: "offeringName",
        excludeKeys: ["reservationId", "customerId", "offeringId", "providerId", "offeringName", "description", "customerFullName", "currency"],
        statusKeyToValue: (value: string) => RESERVATION_STATUSES[value as keyof typeof RESERVATION_STATUSES],
        statusKeyToDescription: (value: string) => RESERVATION_STATUS_DESCRIPTION[value as keyof typeof RESERVATION_STATUS_DESCRIPTION],
        statusKeyToImage: (value: string) => RESERVATION_STATUS_ICON[value as keyof typeof RESERVATION_STATUS_ICON],
        statusKey: "status",
        valueFormatters: {
            price: (value: string, item: ObjectData) => value + ".00 " + item.currency,
            reservationDate: dateValueFormatters,
            reservationDateEnd: dateValueFormatters,
            status: (value: string) => RESERVATION_STATUSES[value as keyof typeof RESERVATION_STATUSES],
        },
        menuItems: {
            "See Reservation": (value: ObjectData) => navigate(MOVE_TO_RESERVATION_OVERVIEW(value.reservationId)),
            "See Provider": (value: ObjectData) => navigate(MOVE_TO_PROVIDER_OVERVIEW(value.providerId)),
        },
        onClickParameter: "reservationId",
        descriptionKey: "description"
    };
}

export default getCustomerReservationMetadata;