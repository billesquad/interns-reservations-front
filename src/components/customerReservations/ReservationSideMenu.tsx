import {Box, Divider, Grid} from "@mui/material";
import BrowseSideMenu from "@/universalComponents/universalSideMenu/BrowseSideMenu";
import MenuSetStatusFilter from "@/components/customerReservations/MenuSetStatusFilter";
import {RESERVATION_STATUSES, RESERVATION_SORT_PARAMS} from "@/components/customerReservations/consts/customerReservationsValues";
import {UrlParams} from "@/fetching/SendApiRequest";

interface ReservationSideMenuProps {
    componentUrlParameters: UrlParams;
    setComponentUrlParameters: (params: UrlParams) => void;
    totalElements: number;
}

const ReservationSideMenu = ({componentUrlParameters, setComponentUrlParameters, totalElements}: ReservationSideMenuProps) => (
    <>
        <Box sx={{width: "100%"}} >
            <Grid
                container
                direction="row"
                justifyContent="flex-start"
                alignItems="center"
                spacing={3}
                sx={{ mb: "2rem" }}
            >
                    <MenuSetStatusFilter
                        componentUrlParameters={componentUrlParameters}
                        setComponentUrlParameters={setComponentUrlParameters}
                        multiselectParameters={RESERVATION_STATUSES}
                    />
                    <BrowseSideMenu
                        sortParameters={RESERVATION_SORT_PARAMS}
                        componentUrlParameters={componentUrlParameters}
                        setComponentUrlParameters={setComponentUrlParameters}
                        totalElements={totalElements}
                    />
            </Grid>
            <Divider/>
        </Box>
    </>
);

export default ReservationSideMenu