export const customerReservationsMainBoxStyle = {
    display: 'flex',
    flexDirection: 'column',
    gap: 5,
    flexGrow: 0,
    alignItems: 'center',
    justifyContent: 'space-between'
}