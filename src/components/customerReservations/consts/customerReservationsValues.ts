import {UrlParams} from "@/fetching/SendApiRequest";
import {AccessTime, Cancel, DoDisturbOn, GroupAdd, TaskAlt} from "@mui/icons-material";

export const RESERVATION_NAMES = {
    "offeringName": "Offering Name",
    "price": "Price",
    "currency": "Currency",
    "description": "Description",
    "providerFullName": "Provider",
    "providerSpecialization": "Provider Specialization",
    "reservationId": "Reservation ID",
    "customerFullName": "Customer",
    "customerId": "Customer ID",
    "offeringId": "Offering ID",
    "reservationDate": "Reservation start date",
    "reservationDateEnd": "Reservation end date",
    "status": "Reservation Status",
}
export type ReservationStatusKey = keyof typeof RESERVATION_STATUSES;
export const RESERVATION_STATUSES = {
    "AWAITING_FOR_APPROVAL": "Awaiting for approval",
    "ACCEPTED": "Accepted",
    "DONE": "Done",
    "CANCELLED": "Cancelled",
    "DECLINED": "Declined"
}

export const INITIAL_URL_PARAMETERS: UrlParams = {
    page: "0",
    size: "3",
    sortParam: "reservationDate",
    direction: "ASC",
    statuses: ["AWAITING_FOR_APPROVAL","ACCEPTED","DONE","CANCELLED","DECLINED"]
}

export const RESERVATION_SORT_PARAMS = {"reservationDate": "Reservation date start", "status": "Status"}

export const RESERVATION_STATUS_DESCRIPTION = {
    "AWAITING_FOR_APPROVAL": "Reservation awaits for approval by provider",
    "ACCEPTED": "Reservation is accepted by provider",
    "DONE": "Reservation was marked as done by provider",
    "CANCELLED": "Reservation was cancelled by customer",
    "DECLINED": "Reservation was declined by provider"
}
export const RESERVATION_STATUS_ICON = {
    "AWAITING_FOR_APPROVAL": AccessTime,
    "ACCEPTED": GroupAdd,
    "DONE": TaskAlt,
    "CANCELLED": Cancel,
    "DECLINED": DoDisturbOn
}