import {
    MenuItem,
    Select,
    SelectChangeEvent,
    FormControl,
    InputLabel,
    OutlinedInput, Checkbox, ListItemText, Grid
} from "@mui/material";
import {useState} from "react";
import {UrlParams} from "@/fetching/SendApiRequest";

interface MyReservationSideMenuProps {
    multiselectParameters: Record<string, string>;
    componentUrlParameters: UrlParams
    setComponentUrlParameters: (params: UrlParams) => void;
}

const MenuSetStatusFilter = ({
                                 multiselectParameters,
                                 componentUrlParameters,
                                 setComponentUrlParameters,
                             }: MyReservationSideMenuProps) => {
    const [selectedMultiSelectValues, setSelectedMultiSelectValues] = useState<string[]>(componentUrlParameters.statuses as string[]);

    const handleChange = (event: SelectChangeEvent<typeof selectedMultiSelectValues>) => {
        const {
            target: {value},
        } = event;
        const newSelectedValues = typeof value === 'string' ? value.split(',') : value;

        setSelectedMultiSelectValues(newSelectedValues);
        setComponentUrlParameters({
            ...componentUrlParameters,
            statuses: newSelectedValues,
        });
    };

    return (
        <>
            <Grid item xs={2}>
                <FormControl fullWidth>
                    <InputLabel id="reservation-status-checkbox-label">Your reservations statuses:</InputLabel>
                    <Select
                        multiple
                        value={selectedMultiSelectValues}
                        onChange={handleChange}
                        input={<OutlinedInput label="Your reservations statuses:"/>}
                        renderValue={(selected) => (selected.map(value => multiselectParameters[value]).join(", "))}
                        id="reservation-status-checkbox"
                        labelId="reservation-status-checkbox-label"
                    >
                        {Object.entries(multiselectParameters)
                            .map((([value, label]) => (
                                <MenuItem key={value} value={value}>
                                    <Checkbox checked={selectedMultiSelectValues.indexOf(value) > -1}/>
                                    <ListItemText primary={label}/>
                                </MenuItem>
                            )))}
                    </Select>
                </FormControl>
            </Grid>
        </>
    )
}
export default MenuSetStatusFilter;
