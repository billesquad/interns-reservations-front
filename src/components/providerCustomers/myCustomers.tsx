import React, { useState, useEffect, useContext } from "react";
import {Paper, Typography, Box, CircularProgress,Select, MenuItem, FormControl, InputLabel, Grid} from '@mui/material';
import SendApiRequest from '@/fetching/SendApiRequest';
import useUserContext from '@/components/auth/useUserContext';
import { Pagination } from '@mui/material';
import { Table, TableBody, TableCell, TableHead, TableRow } from '@mui/material';

import { useNavigate } from 'react-router-dom';
import {CUSTOMER_OVERVIEW} from "@/fetching/urls/internal/internalUrls.ts";


type CustomerResponseDTO = {
    customerId: number;
    addressId: number;
    status: string;
    firstName: string;
    lastName: string;
    birthDate: string;
    email: string;
    phoneNumber: string;
};

type ApiResponse = {
    content: CustomerResponseDTO[];
    totalPages: number;
};

const MyCustomers: React.FC = () => {
    const { userId } = useContext(useUserContext) || {};
    const [customers, setCustomers] = useState<CustomerResponseDTO[]>([]);
    // const [selectedCustomer, setSelectedCustomer] = useState<CustomerResponseDTO | null>(null);
    const [loading, setLoading] = useState<boolean>(true);
    const [currentPage, setCurrentPage] = useState<number>(1);
    const [totalPages, setTotalPages] = useState<number>(0);
    const [rowsPerPage, setRowsPerPage] = useState<number>(10);
    const [sortOrder, setSortOrder] = useState<string>('');

    const navigate = useNavigate();



    useEffect(() => {
        if (!userId) return;

        const fetchCustomersData = async (page: number = 1) => {
            const requestConfig = {
                partOfUrl: `/provider/customers/${userId}?page=${page - 1}&size=${rowsPerPage}`,
            };

            try {
                const response = await SendApiRequest(requestConfig) as ApiResponse;
                if (response && Array.isArray(response.content)) {
                    setCustomers(response.content);
                    setTotalPages(response.totalPages);
                } else {
                    console.error('Expected an object with an array in the content field but received:', response);
                    setCustomers([]);
                }
            } catch (error) {
                console.error('Error fetching customers data:', error);
            } finally {
                setLoading(false);
            }
        };

        fetchCustomersData(currentPage);
    }, [userId, currentPage, rowsPerPage, sortOrder]);

    const handlePageChange = (_: React.ChangeEvent<unknown>, value: number) => {
        setCurrentPage(value);
    };

    const handleRowsPerPageChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setRowsPerPage(event.target.value as number);
        setCurrentPage(1);
    };

    const handleCustomerClick = (customer: CustomerResponseDTO) => {
        navigate(`${CUSTOMER_OVERVIEW}/${customer.customerId}`);
    };

    // const handleCloseModal = () => {
    //     setSelectedCustomer(null);
    // };

    const sortedCustomers = [...customers].sort((a, b) => {
        switch (sortOrder) {
            case 'firstName':
                return a.firstName.localeCompare(b.firstName);
            case 'lastName':
                return a.lastName.localeCompare(b.lastName);
            default:
                return 0;
        }
    });


    return (
        <Paper style={{ padding: '20px', maxWidth: '800px', margin: '20px auto' }}>
            <Grid container spacing={3} alignItems="center" justifyContent="space-between">
                <Grid item>
                    <Typography variant="h5" gutterBottom>
                        My Customers
                    </Typography>
                </Grid>
                <Grid item>
                    <Grid container spacing={2} alignItems="center">
                        <Grid item>
                            <FormControl variant="outlined" size="small" style={{ minWidth: '150px' }}>
                                <InputLabel id="sort-order-label" style={{ fontSize: '0.8rem' }}>Sort by</InputLabel>
                                <Select
                                    labelId="sort-order-label"
                                    id="sort-order"
                                    value={sortOrder}
                                    onChange={(e) => setSortOrder(e.target.value as string)}
                                    label="Sort by"
                                >
                                    <MenuItem value="">None</MenuItem>
                                    <MenuItem value="firstName">First Name</MenuItem>
                                    <MenuItem value="lastName">Last Name</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item>
                            <FormControl variant="outlined" size="small" style={{ minWidth: '150px' }}>
                                <InputLabel id="rows-per-page-label" style={{ fontSize: '0.8rem' }}>Rows per page</InputLabel>
                                <Select
                                    labelId="rows-per-page-label"
                                    id="rows-per-page"
                                    value={rowsPerPage}
                                    onChange={(e) => handleRowsPerPageChange({ target: { value: e.target.value } } as React.ChangeEvent<{ value: unknown }>)}

                                    label="Rows per page"
                                >
                                    <MenuItem value={5}>5</MenuItem>
                                    <MenuItem value={10}>10</MenuItem>
                                    <MenuItem value={25}>25</MenuItem>
                                    <MenuItem value={50}>50</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            {loading ? (
                <Box display="flex" justifyContent="center" marginTop={2}>
                    <CircularProgress />
                </Box>
            ) : (
                <>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>First Name</TableCell>
                                <TableCell>Last Name</TableCell>
                                <TableCell>Email</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {sortedCustomers.map((customer) => (
                                <TableRow
                                    key={customer.customerId}
                                    onClick={() => handleCustomerClick(customer)}
                                    style={{ cursor: 'pointer' }}
                                    hover
                                    sx={{ '&:hover': { backgroundColor: 'rgba(0, 0, 0, 0.04)' } }} // Dostosuj kolor tła podświetlenia
                                >
                                    <TableCell>{customer.firstName}</TableCell>
                                    <TableCell>{customer.lastName}</TableCell>
                                    <TableCell>{customer.email}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                    <Box display="flex" justifyContent="center" marginTop={2}>
                        <Pagination count={totalPages} page={currentPage} onChange={handlePageChange} />
                    </Box>
                </>
            )}
            {/*<Modal*/}
            {/*    open={!!selectedCustomer}*/}
            {/*    onClose={handleCloseModal}*/}
            {/*    aria-labelledby="customer-details-modal"*/}
            {/*    aria-describedby="modal-with-customer-details"*/}
            {/*>*/}
            {/*    <Box sx={{ position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)', bgcolor: 'background.paper', boxShadow: 24, p: 4 }}>*/}
            {/*        {selectedCustomer && <CustomerDetails customer={selectedCustomer} />}*/}
            {/*    </Box>*/}
            {/*</Modal>*/}
        </Paper>
    );
};

export default MyCustomers;
