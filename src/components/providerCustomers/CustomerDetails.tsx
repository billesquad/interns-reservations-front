    import React from 'react';
    import { Paper, Typography, Box } from '@mui/material';

    type CustomerResponseDTO = {
        customerId: number;
        addressId: number;
        status: string;
        firstName: string;
        lastName: string;
        birthDate: string;
        email: string;
        phoneNumber: string;
    };

    type CustomerDetailsProps = {
        customer: CustomerResponseDTO;
    };

    const CustomerDetails: React.FC<CustomerDetailsProps> = ({ customer }) => {
        return (
            <Paper style={{ padding: '20px', maxWidth: '800px', margin: '20px auto' }}>
                <Typography variant="h5" gutterBottom align="center">
                    Customer Details
                </Typography>
                <Box>
                    <Typography variant="body1"><strong>First Name:</strong> {customer.firstName}</Typography>
                    <Typography variant="body1"><strong>Last Name:</strong> {customer.lastName}</Typography>
                    <Typography variant="body1"><strong>Email:</strong> {customer.email}</Typography>
                    <Typography variant="body1"><strong>Phone Number:</strong> {customer.phoneNumber}</Typography>
                    <Typography variant="body1"><strong>Birth Date:</strong> {customer.birthDate}</Typography>
                    <Typography variant="body1"><strong>Status:</strong> {customer.status}</Typography>
                </Box>
            </Paper>
        );
    };

    export default CustomerDetails;
