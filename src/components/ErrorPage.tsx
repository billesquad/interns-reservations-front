import { SIGN_IN } from "@/fetching/urls/internal/internalUrls";
import {Container, Typography, Button, Box} from "@mui/material";
import { useNavigate } from 'react-router-dom';

const ErrorPage = () => {
    const navigate = useNavigate();
    const handleClick = () => navigate(SIGN_IN);

    return(
        <Container>
            <Box sx={{ display: 'flex', flexDirection: 'column', gap: 5, flexGrow: 0, alignItems: 'center', my: 10}}>
                <Typography align="center" variant="h4">
                    This page SHOULD NOT be visible!!!
                </Typography>
                <Button
                    size="large"
                    variant="contained"
                    color="primary"
                    onClick={handleClick}
                >
                    Take me away from here
                </Button>
            </Box>
        </Container>
    )
}
export default ErrorPage;