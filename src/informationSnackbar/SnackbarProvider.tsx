import React, { createContext, useContext, useState } from 'react';
import {Alert, IconButton, Snackbar} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";

type SeverityType = 'success' | 'error' | 'info' | 'warning';

type SnackbarContextType = {
    open: boolean;
    message: string;
    severity: SeverityType;
    openSnackbar: (message: string, severity: 'success' | 'error' | 'info' | 'warning') => void;
    closeSnackbar: () => void;
};

type ChildrenType = {
    children: React.ReactNode;
};

const SnackbarContext = createContext<SnackbarContextType | undefined>(undefined);

export const useSnackbar = () => {
    const context = useContext(SnackbarContext);
    if (!context) {
        throw new Error('useSnackbar must be used within a SnackbarProvider');
    }
    return context;
};

export const SnackbarProvider = ({children}: ChildrenType ) => {
    const [open, setOpen] = useState(false);
    const [message, setMessage] = useState('');
    const [severity, setSeverity] = useState<SeverityType>('info');

    const openSnackbar = (message: string, severity: SeverityType) => {
        setMessage(message);
        setSeverity(severity);
        setOpen(true);
    };

    const closeSnackbar = () => {
        setOpen(false);
    };

    return (
        <SnackbarContext.Provider value={{ open, message, severity, openSnackbar, closeSnackbar }}>
            {children}
            <Snackbar
                open={open}
                autoHideDuration={10000}
                onClose={closeSnackbar}
                action={
                    <IconButton size="medium" aria-label="close" color="inherit" onClick={closeSnackbar}>
                        <CloseIcon fontSize="small" />
                    </IconButton>
                }>
                <Alert onClose={closeSnackbar} severity={severity} sx={{ width: '100%' }}>
                    {message}
                </Alert>
            </Snackbar>
        </SnackbarContext.Provider>
    );
};
