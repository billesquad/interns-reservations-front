import {ChangeEvent, useEffect, useState} from 'react';
import {Select, MenuItem, Grid, TablePagination, InputLabel, FormControl,} from '@mui/material';
import {UrlParams} from "@/fetching/SendApiRequest";

interface BrowseSideMenuProps {
    sortParameters: Record<string, string>;
    componentUrlParameters: UrlParams
    setComponentUrlParameters: (params: UrlParams) => void;
    totalElements: number;
}

const BrowseSideMenu = ({
                            sortParameters,
                            componentUrlParameters,
                            setComponentUrlParameters,
                            totalElements
                        }: BrowseSideMenuProps) => {
    const [currentPage, setCurrentPage] = useState(componentUrlParameters.page);
    const [currentSize, setCurrentSize] = useState(componentUrlParameters.size);
    const [sortParam, setSortParam] = useState(componentUrlParameters.sortParam);
    const [direction, setDirection] = useState(componentUrlParameters.direction);

    useEffect(() => {
        const urlParameters = {
            ...componentUrlParameters,
            "page": currentPage.toString(),
            "size": currentSize.toString(),
            "sortParam": sortParam,
            "direction": direction,
        };
        setComponentUrlParameters(urlParameters)
    }, [currentPage, currentSize, sortParam, direction]);


    // @ts-ignore
    const handleChangePage = (event: unknown, newPage: number,) => {
        setCurrentPage(newPage.toString());
    };

    const handleChangeRowsPerPage = (
        event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,) => {
        setCurrentSize((event.target.value));
        setCurrentPage("0");
    };

    return (
        <>
            <Grid item xs={2}>
                <FormControl fullWidth>
                    <InputLabel id="reservation-sortby-label">Sort By:</InputLabel>
                    <Select
                        value={sortParam}
                        onChange={(e) => setSortParam(e.target.value as string)}
                        labelId="reservation-sortby-label"
                        id="reservation-sortby"
                        label="Sort By:"
                    >
                        {Object.entries(sortParameters).map(([key, value]) => (
                            <MenuItem key={key} value={key}>{value}</MenuItem>
                        ))}
                    </Select>
                </FormControl>
            </Grid>

            <Grid item xs={2}>
                <FormControl fullWidth>
                    <InputLabel id="reservation-direction-label">Direction:</InputLabel>
                    <Select
                        value={direction}
                        onChange={(e) => setDirection(e.target.value as 'ASC' | 'DESC')}
                        labelId="reservation-direction-label"
                        id="reservation-direction"
                        label="Direction:"
                    >
                        <MenuItem value="ASC">Ascending</MenuItem>
                        <MenuItem value="DESC">Descending</MenuItem>
                    </Select>
                </FormControl>
            </Grid>

            <Grid item xs={4} sx={{ml: "15rem"}}>
                <TablePagination
                    component="div"
                    count={totalElements}
                    page={parseInt(currentPage as string)}
                    onPageChange={handleChangePage}
                    rowsPerPage={parseInt(currentSize as string)}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                    color="primary"
                    backIconButtonProps={{color: "primary"}}
                    nextIconButtonProps={{color: "primary"}}
                    rowsPerPageOptions={[3, 6, 9, 12]}
                    labelRowsPerPage="Items per page:"
                />
            </Grid>
        </>
    );
}

export default BrowseSideMenu;
