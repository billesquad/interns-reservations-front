import SendApiRequest, {UrlParams} from "@/fetching/SendApiRequest";
import ObjectData from "@/universalComponents/ObjectDataType";
import {useEffect} from "react";

function useFetchByUrlParams(
    componentUrlParameters: UrlParams,
    partOfUrl: string,
    setFetchedData: (data: ObjectData[]) => void,
    forceUpdate: boolean,
    setTotalElements: (totalElements: number) => void) {
    useEffect(() => {
        if (!partOfUrl) return;
        fetchData();
    }, [componentUrlParameters, forceUpdate]);

    const fetchData = async () => {
        try {
            const data = await SendApiRequest({
                partOfUrl,
                urlParameters: componentUrlParameters,
            });
            setFetchedData(data.content)
            setTotalElements(data.totalElements)
        } catch (error) {
            console.error("Error fetching reservation data:", error);
        }
    }
}

export default useFetchByUrlParams