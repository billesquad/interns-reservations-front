import {createElement, FunctionComponent, useState} from 'react';
import {
    Grid,
    Typography,
    Paper,
    Box,
    Tooltip,
    CardActionArea,
    Menu,
    MenuItem, Divider, List, ListItem, ListItemAvatar, Avatar, ListItemText,
} from '@mui/material';
import ObjectData from '@/universalComponents/ObjectDataType';
import {
    ManyItemsStyleDescription,
    ManyItemsStyleDescriptionBox, ManyItemsStyleItemBody, ManyItemsStyleItemStatus,
    ManyItemsStyleMainPaper
} from "@/universalComponents/universalComponentsStyle";

type MetaData = {
    attributeNames?: ObjectData;
    itemName?: string;
    excludeKeys?: string[];
    menuItems?: Record<string, (props: ObjectData) => void>;
    valueFormatters?: Record<string, (value: string, item: ObjectData) => string>;
    statusKey?: string;
    statusKeyToValue?: (key: string) => string
    statusKeyToDescription?: (key: string) => string
    statusKeyToImage?: (key: string) => FunctionComponent
    descriptionKey?: string;
};

type ManyItemDisplayProps = {
    data: ObjectData[];
    metadata?: MetaData;
};

const ManyItemDisplay = ({data, metadata}: ManyItemDisplayProps) => {
    const excludeKeys = metadata && metadata.excludeKeys ? metadata.excludeKeys : [];
    const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
    const [selectedObject, setSelectedObject] = useState<ObjectData>({});

    const handleItemClick = (event: React.MouseEvent<HTMLElement>, clickedItem: ObjectData) => {
        if (metadata?.menuItems) setAnchorEl(event.currentTarget);
        setSelectedObject(clickedItem);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <Grid container spacing={3}>
            {data.map((item, index) => {
                const itemName = metadata?.itemName && item[metadata.itemName] ? item[metadata.itemName] : "";
                const itemDescription = metadata?.descriptionKey && item[metadata.descriptionKey] ? item[metadata.descriptionKey] : "Provider didn't add a description";
                const status = metadata?.statusKey && item[metadata.statusKey] && metadata.statusKeyToValue ? metadata.statusKeyToValue(item[metadata.statusKey]) : "";
                const statusDescription = (metadata?.statusKey && metadata?.statusKeyToDescription && item[metadata.statusKey]) ? metadata?.statusKeyToDescription(item[metadata.statusKey]) : "";
                const statusImage = metadata?.statusKey && metadata?.statusKeyToImage && item[metadata.statusKey] ? metadata?.statusKeyToImage(item[metadata.statusKey]) : "";

                return (
                    <Grid item xs={12} sm={6} md={4} key={index} flexGrow="2" alignItems="stretch" display="flex"
                          justifyContent="center">
                        <CardActionArea onClick={(e) => handleItemClick(e, item)}>
                            <Paper elevation={3} sx={ManyItemsStyleMainPaper}>
                                {itemName &&
                                    <Box>
                                        <Typography variant="h4" gutterBottom>{itemName}</Typography>
                                    </Box>}
                                {itemDescription &&
                                    <Tooltip title={itemDescription}>
                                        <Box>
                                            <Box sx={ManyItemsStyleDescriptionBox}>
                                                <Typography color="text.secondary"
                                                            variant="body2"
                                                            sx={ManyItemsStyleDescription}>
                                                    {itemDescription}
                                                </Typography>
                                            </Box>
                                            <Divider sx={{mb: "1rem"}}/>
                                        </Box>
                                    </Tooltip>
                                }
                                {Object.entries(item)
                                    .filter(([key]) => !excludeKeys.includes(key))
                                    .map(([itemKey, itemValue]) => {
                                        const displayName = metadata && metadata.attributeNames && metadata.attributeNames[itemKey] ? metadata.attributeNames[itemKey] : itemKey;
                                        const displayValue = metadata && metadata.valueFormatters && metadata.valueFormatters[itemKey] ? metadata.valueFormatters[itemKey](itemValue, item) : itemValue;

                                        return (
                                            <Box key={itemKey} marginBottom={1}>
                                                <Tooltip title={`${displayName}: ${displayValue}`}>
                                                    <Typography variant="body1" sx={ManyItemsStyleItemBody}>
                                                        <strong>{displayName}:</strong> {displayValue as string}
                                                    </Typography>
                                                </Tooltip>
                                            </Box>
                                        );
                                    })}
                                {status &&
                                    <>
                                        <Divider variant="inset" sx={{mt: "0.5rem"}}/>
                                        <List
                                            sx={ManyItemsStyleItemStatus}
                                        >
                                            <ListItem>
                                                <ListItemAvatar>
                                                    <Avatar>
                                                        {createElement(statusImage as unknown as FunctionComponent)}
                                                    </Avatar>
                                                </ListItemAvatar>
                                                <ListItemText primary={status as unknown as string}
                                                              secondary={statusDescription as unknown as string}/>
                                            </ListItem>
                                        </List>
                                    </>}

                            </Paper>
                        </CardActionArea>
                        <Menu
                            anchorEl={anchorEl}
                            open={Boolean(anchorEl)}
                            onClose={handleClose}
                            elevation={3}
                        >
                            {metadata?.menuItems &&
                                Object.entries(metadata.menuItems).map(([label, action], index) => (
                                    <MenuItem key={index} onClick={() => {
                                        if (action) {
                                            action(selectedObject);
                                        }
                                        handleClose();
                                    }}>
                                        {label}
                                    </MenuItem>
                                ))}
                        </Menu>
                    </Grid>
                );
            })}
        </Grid>
    );
};

export default ManyItemDisplay;