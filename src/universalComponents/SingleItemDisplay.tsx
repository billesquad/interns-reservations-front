import { CardContent, Typography, Box, Divider, Link} from '@mui/material';
import ObjectData from "@/universalComponents/ObjectDataType";

export type MetaData = {
    attributeNames?: ObjectData;
    itemName?: string;
    excludeKeys?: string[];
    onClickAttribute?: Record<string, (parameter: string) => void>;
    onClickParameter?: ObjectData;
    valueFormatters?: Record<string, (value: string) => string>;

}
type SingleItemDisplayProps = {
    data: ObjectData
    metadata?: MetaData;
};

const SingleItemDisplay = ({ data, metadata }: SingleItemDisplayProps) => {
    const excludeKeys = metadata && metadata.excludeKeys ?  metadata.excludeKeys : []

    return (
            <CardContent>
                {metadata && metadata.itemName && data[metadata.itemName] &&(
                    <Box display="flex" justifyContent="space-between" alignItems="center" marginBottom={2}>
                        <Typography variant="h6" color="textSecondary">
                            {data[metadata.itemName]}
                        </Typography>
                    </Box>
                )}
                <Divider sx={{ my: 2 }} />

                {Object.entries(data)
                    .filter(([key]) => !excludeKeys.includes(key))
                    .map(([key, value], index) => {
                        const displayName = metadata?.attributeNames && metadata.attributeNames[key] ? metadata.attributeNames[key] : key.charAt(0).toUpperCase() + key.slice(1);
                        const displayValue = metadata && metadata.valueFormatters && metadata.valueFormatters[key] ? metadata.valueFormatters[key](value) : value;

                        const isClickable = metadata?.onClickAttribute?.[key] && metadata?.onClickParameter && metadata?.onClickParameter[key];
                        const callback = metadata?.onClickAttribute?.[key];

                        const content = (
                            <Typography variant="subtitle1">
                                <strong>{displayName}:</strong> {displayValue}
                            </Typography>
                        );

                        return (
                            <Box key={index} marginBottom={1}>
                                {isClickable && callback ?  (
                                    <Link onClick={() => callback(data[metadata?.onClickParameter?.[key] as string])}>
                                        {content}
                                    </Link>
                                ) : content}
                            </Box>
                        )
                    })}
            </CardContent>
    );
};

export default SingleItemDisplay;