export const ManyItemsStyleMainPaper = {
    height: "100%",
    p: '10px',
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between"
};
export const ManyItemsStyleDescription = {
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    display: '-webkit-box',
    WebkitLineClamp: '4',
    WebkitBoxOrient: 'vertical'
};
export const ManyItemsStyleItemBody = {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
};
export const ManyItemsStyleItemStatus = {
    width: '100%',
    maxWidth: 360,
    bgcolor: 'background.paper',
};
export const ManyItemsStyleDescriptionBox = {pr: "5rem", pb: "0.5rem", pl: "0.5rem"}