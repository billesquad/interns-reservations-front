import React, { ReactElement, ReactNode, useContext, useState } from 'react'
import { Link, useNavigate } from "react-router-dom";
import { AppBar, Avatar, Box, Button, IconButton, Menu, MenuItem, Toolbar, Tooltip } from "@mui/material";
import Typography from '@mui/material/Typography';
import UserContext from "@/components/auth/useUserContext";
import USER_COMPONENTS_CONFIG from "@/router/config/userConfig";
import UserRoles from "@/enums/UserRoles";
import { useTheme } from '@mui/material/styles';
import Logo from '@/assets/logo.svg';
import { HOME, SIGN_IN } from '@/fetching/urls/internal/internalUrls';
import SendApiRequest from '@/fetching/SendApiRequest';
import { REVOKE } from '@/fetching/urls/external/authenticationUrls';
import RequestTypes from '@/enums/RequestTypes';
import { useSnackbar } from '@/informationSnackbar/SnackbarProvider';

type NavigationConfig = {
    name: string;
    path: string;
    page: ReactNode | (() => ReactElement);
    requiredRole: UserRoles;
};

type NavbarObject = {
    navigationConfig: NavigationConfig[];
    setComponentView: (value: ReactNode) => void
};

const Navbar = ({ navigationConfig }: NavbarObject) => {
    const navItems = navigationConfig;
    const settings = USER_COMPONENTS_CONFIG;
    const [anchorElUser, setAnchorElUser] = useState<null | HTMLElement>(null);
    const { avatarUrl } = useContext(UserContext);
    const navigate = useNavigate();
    const theme = useTheme();
    const { openSnackbar } = useSnackbar();
    const { removePayloadCookie } = useContext(UserContext);

    const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorElUser(event.currentTarget);
    };

    const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    };

    const handleUserMenuItemClick = (path: string) => {
        navigate(path)
        setAnchorElUser(null);
    };

    const handleNavbarClick = (path: string) => {
        navigate(path)
    };

    const handleLogout = async () => {
        try {
            await SendApiRequest({
                partOfUrl: REVOKE,
                typeOfRequest: RequestTypes.GET
            });
            removePayloadCookie();

            navigate(SIGN_IN);
            openSnackbar('Logged out.', 'success');
        }
        catch (error) {
            console.error('Error while logging out:', error);
        }
    }
    
    return (
        <>
            <AppBar component="nav" sx={{ backgroundColor: theme.palette.background.default }}>
                <Toolbar>
                    <Link to={HOME}>
                        <img src={Logo} alt="Reservations Logo" />
                    </Link>
                    <Box sx={{ flexGrow: 1, ml: 3, display: { xs: 'none', md: 'flex' } }}>
                        {navItems.map((navItem) => (
                            <Button
                                variant="text"
                                onClick={() => handleNavbarClick(navItem.path)}
                                key={navItem.name}
                                sx={{
                                    my: 2,
                                    ml: 2,
                                    color: 'black',
                                    display: 'block',
                                    fontWeight: 'bold',
                                    '&:hover': {
                                        backgroundColor: 'transparent',
                                        textDecoration: 'none'
                                    }
                                }}
                            >
                                {navItem.name}
                            </Button>
                        ))}
                    </Box>
                    <Box sx={{ flexGrow: 0 }}>
                        <Tooltip title="Open settings">
                            <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                                <Avatar alt="User avatar" src={avatarUrl} />
                            </IconButton>
                        </Tooltip>
                        <Menu
                            sx={{ mt: '45px' }}
                            id="menu-appbar"
                            anchorEl={anchorElUser}
                            anchorOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            open={Boolean(anchorElUser)}
                            onClose={handleCloseUserMenu}
                        >
                            {settings.map((setting) => (
                                <MenuItem key={setting.name} onClick={() => handleUserMenuItemClick(setting.path)}>
                                    <Typography textAlign="center">{setting.name}</Typography>
                                </MenuItem>
                            ))}
                            <MenuItem key={"logout"} onClick={() => handleLogout()}>
                                <Typography textAlign="center">Logout</Typography>
                            </MenuItem>
                        </Menu>
                    </Box>
                </Toolbar>
            </AppBar>
        </>
    )
}

export default Navbar;