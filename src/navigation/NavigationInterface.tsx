import AppRouter from "@/router/MainRouter";
import Navbar from "@/navigation/Navbar";
import { Box, Paper } from "@mui/material";
import { ReactNode, useState } from "react";
import UserContext from "@/components/auth/useUserContext";
import UserRoles from "@/enums/UserRoles";
import PROVIDER_COMPONENTS_CONFIG from "@/router/config/providerConfig";
import CUSTOMER_COMPONENTS_CONFIG from "@/router/config/customerConfig";
import UserContextType from "@/components/auth/userContextType";

const NavigationInterface = ({cookies, removePayloadCookie, isCookieSet, userRole, avatarUrl, userId}: UserContextType) => {
    const [, setComponentView] = useState<ReactNode>();

    let navigationConfig = PROVIDER_COMPONENTS_CONFIG;
    if (userRole === UserRoles.ROLE_CUSTOMER) {
        navigationConfig = CUSTOMER_COMPONENTS_CONFIG;
    }
    return (
        <>
            <UserContext.Provider value={{
                cookies,
                removePayloadCookie,
                isCookieSet,
                userRole,
                avatarUrl,
                userId}}>
            {(userRole === UserRoles.ROLE_PROVIDER || userRole === UserRoles.ROLE_CUSTOMER)
                && <Navbar navigationConfig={navigationConfig} setComponentView={setComponentView}></Navbar>}
                <Box
                    sx={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        pt: 12,
                        pb: 5,
                    }}
                >
                    <Paper
                        elevation={3}
                        sx={{
                            minHeight: '85vh',
                            padding: 5,
                            width: '95%',
                            height: 'auto'
                        }}
                    >
                        <AppRouter />
                    </Paper>
                </Box>
            </UserContext.Provider>
        </>
    );
};
export default NavigationInterface;