import { Route, Routes } from 'react-router-dom';
import GuardedRoute from "@/router/GuardedRoute";
import APP_ROUTER_CONFIG from "@/router/config/mainRouterConfig";
import USER_COMPONENTS_CONFIG from "@/router/config/userConfig";
import PROVIDER_COMPONENTS_CONFIG from "@/router/config/providerConfig";
import CUSTOMER_COMPONENTS_CONFIG from "@/router/config/customerConfig";
import PageTitleSetter from "@/router/PageTitleSetter";
import UserRoles from '@/enums/UserRoles';
import {ElementType} from "react";

interface RouteConfig {
    path: string;
    page: ElementType;
    requiredRole: UserRoles | UserRoles[] | string;
    title?: string;
}

const AppRouter = () => {``
    const combinedConfig: RouteConfig[] = [
        ...APP_ROUTER_CONFIG,
        ...USER_COMPONENTS_CONFIG,
        ...PROVIDER_COMPONENTS_CONFIG,
        ...CUSTOMER_COMPONENTS_CONFIG
    ];

    return (
        <Routes>
            {combinedConfig.map((route, index) => (
                <Route key={index} path={route.path} element={
                    <>
                        <PageTitleSetter title={route.title || "Reservations"} />
                        <GuardedRoute requiredRoles={Array.isArray(route.requiredRole) ? route.requiredRole : [route.requiredRole]}>
                            <route.page />
                        </GuardedRoute>
                    </>
                } />
            ))}
        </Routes>
    );
};

export default AppRouter;