import { Navigate } from "react-router-dom";
import { useContext } from "react";
import UserContext from "@/components/auth/useUserContext";
import UserRoles from "@/enums/UserRoles";
import { ERROR, HOME, REGISTER, SIGN_IN } from "@/fetching/urls/internal/internalUrls";

interface GuardedRouteProps {
    requiredRoles: string[];
    children: React.ReactNode;
}

const GuardedRoute: React.FC<GuardedRouteProps> = ({ children, requiredRoles }) => {
    const { userRole } = useContext(UserContext);

    const getProperRoute = () => {
        switch (userRole) {
            case UserRoles.NON_LOGGED:
                return SIGN_IN;
            case UserRoles.NON_REGISTERED:
                return REGISTER;
            case UserRoles.ROLE_CUSTOMER:
            case UserRoles.ROLE_PROVIDER:
                return HOME;
            default:
                return ERROR;
        }
    };

    return (requiredRoles && !requiredRoles.includes(userRole as string)
        ? <Navigate to={getProperRoute()} replace />
        : children
    );
};

export default GuardedRoute;