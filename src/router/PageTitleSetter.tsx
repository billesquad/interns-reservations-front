import { useEffect } from "react";

interface PageTitleSetterProps {
    title: string;
}

const PageTitleSetter: React.FC<PageTitleSetterProps> = ({ title }) => {
    useEffect(() => {
        document.title = title;
    }, [title]);

    return null;
};

export default PageTitleSetter;
