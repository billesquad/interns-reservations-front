import ErrorPage from "@/components/ErrorPage";
import UserRoles from "@/enums/UserRoles";
import SignInPage from "@/components/signIn/SignInPage";
import Home from "@/components/home/Home";
import FirstLogged from "@/components/firstLogged/FirstLogged";
import UserOverview from "@/components/userOverwiev/UserOverview";
import OfferingReservation from "@/components/offeringReservation/OfferingReservation";
import { CUSTOMER_OVERVIEW, ERROR, HOME, OFFERING_RESERVE, PROVIDER_OVERVIEW, REGISTER, RESERVATION, SIGN_IN } from "@/fetching/urls/internal/internalUrls";
import Reservation from "@/components/reservation/ReservationDetails";

const APP_ROUTER_CONFIG = [
    { path: SIGN_IN, page: SignInPage, requiredRole: UserRoles.NON_LOGGED, title: "Sign In" },
    { path: HOME, page: Home, requiredRole: [UserRoles.ROLE_CUSTOMER, UserRoles.ROLE_PROVIDER], title: "Home" },
    { path: REGISTER, page: FirstLogged, requiredRole: UserRoles.NON_REGISTERED, title: "Register" },
    { path: ERROR, page: ErrorPage, requiredRole: "", title: "Error" },
    { path: PROVIDER_OVERVIEW + '/:id', page: UserOverview, requiredRole: UserRoles.ROLE_CUSTOMER, title: "Provider Overview" },
    { path: CUSTOMER_OVERVIEW + '/:id', page: UserOverview, requiredRole: UserRoles.ROLE_PROVIDER, title: "Customer Overview" },
    { path: OFFERING_RESERVE + '/:id', page: OfferingReservation, requiredRole: UserRoles.ROLE_CUSTOMER, title: "Reserve" },
    { path: RESERVATION + '/:id', page: Reservation, requiredRole: [UserRoles.ROLE_CUSTOMER, UserRoles.ROLE_PROVIDER], title: "Reservation Details" },
];

export default APP_ROUTER_CONFIG;