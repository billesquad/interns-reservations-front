import UserRoles from "@/enums/UserRoles";
import ProviderOfferings from "@/components/providerOfferings/ProviderOfferings";
import ProviderReservations from "@/components/providerReservations/ProviderReservations";
import { CUSTOMERS_PROVIDER, OFFERINGS_PROVIDER, RESERVATIONS_PROVIDER, WORKHOURS_PROVIDER } from "@/fetching/urls/internal/internalUrls";
import MyCustomers from "@/components/providerCustomers/myCustomers.tsx";
import ProviderCalendar from "@/components/providerCalendar/ProviderCalendar";

const PROVIDER_COMPONENTS_CONFIG = [
    { name: 'My Work Hours', path: WORKHOURS_PROVIDER, page: ProviderCalendar, requiredRole: UserRoles.ROLE_PROVIDER },
    { name: 'My Offerings', path: OFFERINGS_PROVIDER, page: ProviderOfferings, requiredRole: UserRoles.ROLE_PROVIDER, title: "My Offerings" },
    { name: 'My Reservations', path: RESERVATIONS_PROVIDER, page: ProviderReservations, requiredRole: UserRoles.ROLE_PROVIDER, title: "My Reservations" },
    { name: 'My Customers', path: CUSTOMERS_PROVIDER, page: MyCustomers, requiredRole: UserRoles.ROLE_PROVIDER },
];

export default PROVIDER_COMPONENTS_CONFIG;