import UserRoles from "@/enums/UserRoles";
import OfferingBrowser from "@/components/offeringBrowser/OfferingBrowser";
import CustomerReservations from "@/components/customerReservations/CustomerReservations";
import {OFFERINGS_CUSTOMER, RESERVATIONS_CUSTOMER} from "@/fetching/urls/internal/internalUrls";

const CUSTOMER_COMPONENTS_CONFIG = [
    { name: 'Browse Offerings', path: OFFERINGS_CUSTOMER, page: OfferingBrowser, requiredRole: UserRoles.ROLE_CUSTOMER, title: "Browse Offerings" },
    { name: 'My Reservations', path: RESERVATIONS_CUSTOMER, page: CustomerReservations, requiredRole: UserRoles.ROLE_CUSTOMER, title: "My Reservations" },
];

export default CUSTOMER_COMPONENTS_CONFIG;