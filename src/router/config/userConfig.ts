import UserForm from "@/components/profileForm/UserForm";
import UserRoles from "@/enums/UserRoles";
import { PROFILE } from "@/fetching/urls/internal/internalUrls";

const USER_COMPONENTS_CONFIG = [
    { name: 'Profile', path: PROFILE, page: UserForm, requiredRole: [UserRoles.ROLE_PROVIDER, UserRoles.ROLE_CUSTOMER] },
];

export default USER_COMPONENTS_CONFIG;