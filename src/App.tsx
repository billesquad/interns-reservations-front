import UserAuthInfo from "@/components/auth/UserAuthInfo";
import {CssBaseline} from "@mui/material";
import { createTheme, ThemeProvider } from '@mui/material';

const theme = createTheme({
    palette: {
        background: {
            default: '#FAFAFA'
        },
        primary: {
            main: '#FF5900'
        },
        secondary: {
            main: '#FF8000'
        }
    }
});

function App() {
  return (
      <ThemeProvider theme={theme}>
          <CssBaseline/>
          <UserAuthInfo/>
      </ThemeProvider>
  );
}

export default App;
