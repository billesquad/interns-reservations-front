import SendApiRequest, {RequestConfig} from "@/fetching/SendApiRequest";

type UserDataObject = {
    requestConfig: RequestConfig
}
const fetchData = async ({requestConfig}: UserDataObject) => {
    try {
        const response = await SendApiRequest(requestConfig);
        return response;
    } catch (error) {
        return null;
    }
};

export default fetchData;