const RESPONSE_TYPE = "code";
const CLIENT_ID = "833124940000-2g0ihrs74vqhipt0b1eiiq32ih6dvqhc.apps.googleusercontent.com";
const SCOPE = "https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile";
const REDIRECT_URI = "http%3A%2F%2Flocalhost%3A8080%2Flogin%2Foauth2%2Fcode%2Fgoogle";
const PROMPT = "consent";
const ACCES_TYPE = "offline";
const GOOGLE_URL = "https://accounts.google.com/o/oauth2/auth";

export const AUTH_URL = `${GOOGLE_URL}?response_type=${RESPONSE_TYPE}&client_id=${CLIENT_ID}&scope=${SCOPE}&redirect_uri=${REDIRECT_URI}&prompt=${PROMPT}&access_type=${ACCES_TYPE}`; 

export const BASE_URL = "http://localhost:8080";
