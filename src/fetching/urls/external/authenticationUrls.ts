export const AUTHORIZE = `/login/oauth2/code/google`;
export const REVOKE = `/revoke`;
export const REGISTER_CUSTOMER = `/register/customer`;
export const REGISTER_PROVIDER = `/register/provider`;
export const GET_ROLE = `/my-data`;
export const REMOVE_USER_API_URL = `/user/delete/`
export const REMOVE_USER = (id: string) => `${REMOVE_USER_API_URL}${id}`;