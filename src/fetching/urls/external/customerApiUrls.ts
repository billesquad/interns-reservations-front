const CUSTOMER_API_URL = "/customer";

export const DELETE_CUSTOMER_BY_ID = (id: string) => `${CUSTOMER_API_URL}/delete/${id}`;
export const UPDATE_CUSTOMER_BY_ID = (id: string) => `${CUSTOMER_API_URL}/update/${id}`;

const CUSTOMER_RESERVATION_API_URL = "/customer/reservation";

export const CREATE_RESERVATION = `${CUSTOMER_RESERVATION_API_URL}`;
export const CANCEL_RESERVATION = (id: string) => `${CUSTOMER_RESERVATION_API_URL}/cancel/${id}`;
export const GET_CUSTOMER_RESERVATIONS = (id: string) => `${CUSTOMER_RESERVATION_API_URL}/all/${id}`;
