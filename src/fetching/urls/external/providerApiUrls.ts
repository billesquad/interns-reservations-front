const PROVIDER_API_URL = "/provider";

export const DELETE_PROVIDER_BY_ID = (id: string) => `${PROVIDER_API_URL}/delete/${id}`;
export const UPDATE_PROVIDER_BY_ID = (id: string) => `${PROVIDER_API_URL}/update/${id}`;
export const GET_CUSTOMERS_BY_PROVIDER_ID = (id: string) => `${PROVIDER_API_URL}/customers/${id}`;

const PROVIDER_OFFERING_API_URL = "/provider/offering";

export const CREATE_OFFERING = `${PROVIDER_OFFERING_API_URL}`;
export const DELETE_OFFERING_BY_ID = (id: string) => `${PROVIDER_OFFERING_API_URL}/delete/${id}`;
export const UPDATE_OFFERING_BY_ID = (id: string) => `${PROVIDER_OFFERING_API_URL}/update/${id}`;
export const GET_OFFERINGS_BY_PROVIDER_ID = (id: string) => `${PROVIDER_OFFERING_API_URL}/get-by-provider/${id}`;

const PROVIDER_RESERVATION_API_URL = "/provider/reservation";

export const GET_RESERVATIONS_BY_PROVIDER_ID = (id: string) => `${PROVIDER_RESERVATION_API_URL}/all/${id}`;
export const CHANGE_RESERVATION_STATUS = (id: string) => `${PROVIDER_RESERVATION_API_URL}/change-status/${id}`;

const PROVIDER_WORK_HOUR_API_URL = "/provider/work-hour";

export const UPDATE_WORK_HOUR = (id: string) => `${PROVIDER_WORK_HOUR_API_URL}/update/${id}`;
export const DELETE_WORK_HOUR = (id: string) => `${PROVIDER_WORK_HOUR_API_URL}/delete/${id}`;