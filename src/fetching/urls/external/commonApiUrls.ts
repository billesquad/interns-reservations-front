const ADDRESS_API_URL = "/common/address";

export const GET_ADDRESS_BY_ID = (id: string) => `${ADDRESS_API_URL}/${id}`;
export const UPDATE_ADDRESS = (id: string) => `${ADDRESS_API_URL}/update/${id}`;
export const DELETE_ADDRESS = (id: string) => `${ADDRESS_API_URL}/delete/${id}`;

const OFFERING_API_URL = "/common/offering";

export const GET_OFFERING_BY_ID = (id: string) => `${OFFERING_API_URL}/${id}`;
export const GET_OFFERINGS = `${OFFERING_API_URL}/all`;
export const GET_PROVIDER_BY_OFFERING_ID = (id: string) => `${OFFERING_API_URL}/${id}/provider`;

const RESERVATION_API_URL = "/common/reservation";

export const GET_RESERVATIONS_BY_PROVIDER_AND_DAY = `${RESERVATION_API_URL}/times-by-day`;
export const GET_RESERVATION_BY_ID = (id: string) => `${RESERVATION_API_URL}/${id}`;

const USER_API_URL = "/common";

export const GET_CUSTOMER_BY_ID = (id: string) => `${USER_API_URL}/customer/${id}`;
export const GET_PROVIDER_BY_ID = (id: string) => `${USER_API_URL}/provider/${id}`;

const WORK_HOUR_API_URL = "/common/work-hour";

export const GET_WORKHOUR_BY_ID = (id: string) => `${WORK_HOUR_API_URL}/${id}`;
export const GET_PROVIDER_CALENDAR = (id: string) => `${WORK_HOUR_API_URL}/calendar/${id}`;
export const GET_PROVIDER_CALENDAR_BY_OFFERING_ID = (id: string) => `${WORK_HOUR_API_URL}/offering/${id}`;