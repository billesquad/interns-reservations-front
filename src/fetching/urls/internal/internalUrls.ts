export const OFFERINGS_CUSTOMER = "/offerings-customer";
export const OFFERING_RESERVE = "/offering-customer";
export const PROVIDER_OVERVIEW = "/provider";
export const CUSTOMER_OVERVIEW = "/customer";
export const RESERVATIONS_CUSTOMER = "/reservations-customer";

export const OFFERINGS_PROVIDER = "/offerings-provider";
export const RESERVATIONS_PROVIDER = "/reservations-provider";
export const WORKHOURS_PROVIDER = "/workhours-provider";


export const CUSTOMERS_PROVIDER = "/myCustomers";

export const PROFILE = "/profile";

export const SIGN_IN = "/";
export const HOME = "/home";
export const REGISTER = "/register";
export const ERROR = "/error";
export const RESERVATION = "/reservation";
export const MOVE_TO_RESERVATION_OVERVIEW = (id: string) => RESERVATION + "/" + id;
export const MOVE_TO_PROVIDER_OVERVIEW = (id: string) => `/provider/${id}`;
export const MOVE_TO_CUSTOMER_OVERVIEW = (id: string) => `/customer/${id}`;


