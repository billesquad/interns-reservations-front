import axios, {AxiosRequestConfig, AxiosResponse} from "axios";
import { BASE_URL } from "@/fetching/urls/external/configUrls";
import RequestTypes from "@/enums/RequestTypes";

export type UrlParams = Record<string, string | string[]>;

export interface RequestConfig {
    partOfUrl?: string;
    typeOfRequest?: string;
    urlParameters?: UrlParams;
    body?: Record<string, unknown>;
}

const DEFAULT_METHOD = RequestTypes.GET;

const constructURL = ({ partOfUrl, urlParameters }: RequestConfig) => {
    let url = `${BASE_URL}${partOfUrl || ''}`;
    if (urlParameters) {
        const params = new URLSearchParams();

        for (const [key, value] of Object.entries(urlParameters)) {
            if (Array.isArray(value)) {
                for (const v of value) {
                    params.append(key, v);
                }
            } else {
                params.append(key, value);
            }
        }
        url += `?${params}`;
    }
    return url;
}

async function SendApiRequest(config: RequestConfig, openSnackbar?: (message: string, variant: 'success' | 'error') => void) {
    const { partOfUrl, typeOfRequest, urlParameters, body } = config;

    const axiosConfig: AxiosRequestConfig = {
        withCredentials: true,
        method: typeOfRequest || DEFAULT_METHOD,
        url: constructURL({ partOfUrl, urlParameters }),
        data: body
    };

    try {
        const response = await axios(axiosConfig);
        handleResponse(response, openSnackbar);
        return response.data;
    } catch (error: unknown) {
        handleAxiosError(error as Error, openSnackbar);
        throw error;
    }
}

const handleResponse = (response: AxiosResponse, openSnackbar?: (message: string, variant: 'success' | 'error') => void) => {
    if (response.status >= 200 && response.status < 300) {
        openSnackbar?.('Operation was successful!', 'success');
    }
}

const handleAxiosError = (error: Error, openSnackbar?: (message: string, variant: 'success' | 'error') => void) => {
    let errorMessage = 'Error making request!';
    if (axios.isAxiosError(error)) {
        errorMessage = error.response?.data?.error || errorMessage;
    }
    openSnackbar?.(errorMessage, 'error');
    //console.error('Error making request:', error);
}

export default SendApiRequest;
