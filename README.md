# Interns Reservations

<div align="center">
  <p>
    <img src="https://gitlab.com/billesquad/interns-reservations-front/-/raw/main/src/assets/logo.svg" alt="Logo" height="150" />
  </p>
</div>

## Description
As a team of interns, we started our adventure with creating a React application using our API for booking services. Using the latest technology and mentor support, our goal is to provide a user-friendly solution that meets a variety of booking needs.

# Installation

## Prerequisites:

* <b>Interns Reservations (backend)</b>: Configure and run the server based on: [Our Reservations Project](https://gitlab.com/billesquad/interns-reservations)

* <b>Node.js</b>: Ensure you have Node.js installed (and NPM). If not, download and install it from the official Node Js website.

* <b>Typescript</b>: The project is written in TypeScript. Install it globally (npm install -g typescript) or install it locally in your project.

### Setup

Clone the Repository:
* <b>git clone https://gitlab.com/billesquad/interns-reservations-front </b>
* <b>cd interns-reservations-front</b>

### Dependency Installation:
Run the following command to download and install the required dependencies:
* <b>npm install</b>

### Run the Application:
Run the following command to run the application:
* <b>npm run dev</b>

## Features
* OAuth2 authentication/authorization
* Cookie based session
* Integration with Google API for additional features
* JWT for secure data transmission
